<?php
include("../apiconfig.php");
$from = date("Y-m-d")." 00:00:00";
//$from = "2017-02-23 00:00:00";
$to = date("Y-m-d")." 23:59:59";
$getData = mysqli_query($con,"SELECT * FROM `notifications` WHERE (`to` = '$loggeduserid' OR `all` = '1') AND `createdate` BETWEEN '$from' AND '$to' ORDER BY `id` DESC") or die(mysqli_error($con));
?>


<div class="modal-header" style="background:#c9302c;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:#fff;">NOTIFICATIONS</h4>
      </div>
      <div class="modal-body">
      
    <center>

<?php
if(mysqli_num_rows($getData) > 0)
{
	?>

	<table class="notifDocks">
	<?php
while($row = mysqli_fetch_array($getData))
{
	if($row['type'] == 'DIARY')
	{
		$class = 'fa fa-file diary';
		$onclick="changePage('viewThree','templates/viewThree.html','templates/diary.php?id=".$row['student']."','viewThreeRes','','strong','homeHeader');globeTitle = 'DIARY'; globeTitleId = 'viewthreetitle';";
	}
	else if($row['type'] == 'MESSAGE')
	{
		$class = 'fa fa-envelope message';
				$onclick="changePage('viewThree','templates/viewThree.html','templates/messages.php?id=".$row['student']."','viewThreeRes','','strong','homeHeader');globeTitle = 'MESSAGES'; globeTitleId = 'viewthreetitle'";

	}
	else if($row['type'] == 'CIRCULAR')
	{
		$class = 'fa fa-globe remark';
		$onclick="changePage('viewThree','templates/viewThree.html','templates/circulars.php','viewThreeRes','','strong','homeHeader');globeTitle = 'CIRCULARS'; globeTitleId = 'viewthreetitle'";

	}
	else if($row['type'] == 'FEES')
	{
		$class = 'fa fa-credit-card remark';
		$onclick="changePage('viewThree','templates/viewThree.html','templates/fees.php','viewThreeRes','','strong','homeHeader');globeTitle = 'FEES'; globeTitleId = 'viewthreetitle'";

	}

	else if($row['type'] == 'ATTENDANCE')
	{
		$class = 'fa fa-th-large remark';
		$onclick="changePage('viewThree','templates/viewThree.html','templates/fees.php','viewThreeRes','','strong','homeHeader');globeTitle = 'FEES'; globeTitleId = 'viewthreetitle'";

	}

else if($row['type'] == 'BUS1' || $row['type'] == 'BUS2' || $row['type'] == 'BUS3' || $row['type'] == 'BUS4')
	{
		$class = 'fa fa-map-marker marker';
		$onclick="changePage('viewThree','templates/viewThree.html','templates/bus.php?id=".$row['student']."','viewThreeRes','','strong','homeHeader');globeTitle = 'BUS DETAILS '; globeTitleId = 'viewthreetitle'";

	}
	?>
<tr>
		<td>
		<i class="<?php echo $class;?>"></i>
		<br/>
		
		<span style="font-size:11px;"><?php echo date("d-M",strtotime($row['createdate']));?></span>
		</td>
		<td>
		<div class="dot"></div>
		<?php echo $row['notification'];?>
		<br/>
		<span class="timeLine"><?php echo date("h:i A",strtotime($row['createdate']));?></span>
		</td>
	</tr>
	<?php
}
?>
</table>
<?php
}

else
{
	?>
<center>
	<div>
	<br/><br/><br/>
<i class="fa fa-coffee" style="font-size:60px;color:#c9302c"></i>
<br/>
<br/>
No notificaitons from today yet.
<br/>
<br/>

	</div>
</center>
	<?php
}
?>
	
	




</center>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>


