<?php
include("../apiconfig.php");
$id = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `busgps` WHERE `busid` = '$id' ORDER BY `id` DESC LIMIT 8") or die(mysqli_error($con));
?>
<!DOCTYPE html>
<html>
<head>
	<script src="../../client/scripts/jquery.min.js" type="text/javascript"></script>
</head>
<body style="margin:0px;padding:0px">

<div id="map" style="width:100%;"></div>
<script>

var latArray = [];
var longArray = [];


<?php
$j=0;
while($rowData = mysqli_fetch_array($getData))
{
  ?>
latArray[<?php echo $j;?>] = <?php echo $rowData['lat'];?>;
longArray[<?php echo $j;?>] = <?php echo $rowData['lng'];?>;
  <?php
  $j++;
}

?>


var marker;
var myCenter;
var map;
function myMap() {
  myCenter = new google.maps.LatLng(latArray[7],longArray[7]);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 13, };
  map = new google.maps.Map(mapCanvas, mapOptions);


image = {
  url: '../../client/pin-live.svg',
  size: new google.maps.Size(100, 100 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(40,40),
  scaledSize: new google.maps.Size(100, 100)
};



marker = new google.maps.Marker({
    position: myCenter,
    map: map,
    optimized: false,
    icon:image
  });


//  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);

  showMagic();
}

function placeMarker(lat,lng)
{
  myCenter = new google.maps.LatLng(lat,lng);

marker.setMap(null);


marker = new google.maps.Marker({
    position: myCenter,
    map: map,
    optimized: false,
    icon:image
  });


//  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);


}


function showMagic()
{
  setTimeout(function(){
    placeMarker(latArray[6],longArray[6]);
  },3000);


  setTimeout(function(){
    placeMarker(latArray[5],longArray[5]);
  },6000);


  setTimeout(function(){
    placeMarker(latArray[4],longArray[4]);
  },9000);

  setTimeout(function(){
    placeMarker(latArray[3],longArray[3]);
  },12000);
  setTimeout(function(){
    placeMarker(latArray[2],longArray[2]);
  },15000);
  setTimeout(function(){
    placeMarker(latArray[1],longArray[1]);
  },18000);
  setTimeout(function(){
    placeMarker(latArray[0],longArray[0]);
  },21000);

}

setInterval(function(){
getAutoCall();
},30000);

function getAutoCall()
{
  $.get("getnewlive.php?id=<?php echo $_GET['id'];?>", function(data, status){
        var res = data.split("***");
        placeMarker(res[0],res[1]);
    });
}






</script>


<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=myMap"></script>

<script type="text/javascript">
	var inHeight = window.innerHeight;
	$('#map').height(inHeight);


</script>
</body>
</html>
