<?php
session_start();
ob_start();

///error_reporting(0);
$ip = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
$token = time().rand(100000,9999999).$ip;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>.::SCHOOLBIRD CLIENT LOGIN::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>  

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
<script type="text/javascript">
function loginNow()
{
  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  if(username == '' || password == '')
  {
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      
      return false;
  }
  var url = "login.php";
  var params = {
    username:username,
    password:password
  }
  document.getElementById('logButton').innerHTML = 'AUTHENTICATING..';
  genAjax(url,params,function(response){
    if(response == '0')
    {
      document.getElementById('logButton').innerHTML = 'RETRY LOGIN <i class="fa fa-arrow-right"></i>';
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      

    }
    else
    {
       
      document.getElementById('logButton').innerHTML = 'REDIRECTING..';
      if(username == 'admin'|| username == 'Admin@123' || username == 'Admindps')
      {
        window.location = 'default.php';
      }
      else
      {

      $('#login_window_1').hide();
      $('#login_window_2').show();
    }
     //window.location = "default.php";
    }
  });

}


function sendOtp()
{
  var username = document.getElementById('username').value;
  var mobile = document.getElementById('mobile').value;

  if(mobile == '')
  {
      toast("How can we send an OTP on a blank mobile number?",'toast bg-danger','3000');      
      return false;
  }
  else if(mobile < 7000000000 || mobile > 9999999999)
  {
      toast("Invalid mobile number. Call you friend from your mobile and ask them to tell you the number displayed on your screen when your call was appearing.",'toast bg-danger','10000');    
      return false;    
  }
  else if(isNaN(mobile))
  {
      toast("Invalid mobile number. You really have "+mobile+" as your number? Seriously? With alphabets or symbols in it?",'toast bg-danger','10000');      
      return false;    
  }
  else if(mobile != 8966003469 ) /// 8966002864
  {
      toast("Unauthorized mobile number. Please contact administrator for authorized mobile numbers.",'toast bg-danger','3000');      
      return false;

  }
  var url = "sendotp.php";
  var params = {
    mobile:mobile,
    username:username,
    token:'<?php echo $token; ?>'
  }
  document.getElementById('optbutton').innerHTML = 'SENDING OTP..';
  
  genAjax(url,params,function(response){
       document.getElementById('optbutton').innerHTML = 'OTP SENT..MOVING NOW';
        setTimeout(function(){
     
      $('#login_window_2').hide();
      $('#login_window_3').show();
            },1000);



      setTimeout(function(){
              document.getElementById('optbutton').innerHTML = 'SEND OTP <i class="fa fa-arrow-right"></i>';
            },2000);
  });

  
}

function resend()
{
  document.getElementById('mobile').value = '';
         $('#login_window_2').show();
      $('#login_window_3').hide(); 
}



function verifyOtp()
{
  var username = document.getElementById('username').value;
  var mobile = document.getElementById('mobile').value;
  var otp = document.getElementById('otp').value;
  if(otp == '')
  {
      toast("Invalid OTP. Did we really send a blank OTP?",'toast bg-danger','3000');      
      return false;
  }
  var url = "checkOtp.php";
  var params = {
    mobile:mobile,
    username:username,
    token:'<?php echo $token; ?>',
    otp:otp
  }
  document.getElementById('confirmbutton').innerHTML = 'CHECKING OTP..';
  
  genAjax(url,params,function(response){
    if(response.indexOf('FALSE') == -1)
    {
       document.getElementById('confirmbutton').innerHTML = 'REDIRECTING..';
       window.location = 'default.php';
    }
    else
    {
            toast("Invalid OTP. Please try again.",'toast bg-danger','3000'); 
        document.getElementById('confirmbutton').innerHTML = 'VERIFY <i class="fa fa-arrow-right"></i>';     
    }

  });

  
}

</script>
</head>
<body>
<div class="row footer">
  <div class="col-sm-12" style="font-size:10px;color:#888">
  <center>
    <hr>
    Copyright 2016, All Rights Reserved.<br/>
    <a href="#">
    AcesSkyNet Pvt Ltd.
    </a>
    <br/>
    <br/>
  </center>
  </div>
</div>
<div class="row">
  <div class="col-sm-12 logHeadBack" style="text-align:center;padding-top:40px;padding-bottom:80px;">
    <div style="width:350px;text-align:left;display:inline-block;color:#fff;">
    <img src="images/logo.png" alt="" style="width:60%;">
<br/><br/>
    <h2>Welcome to SchoolBird client portal.</h2>
    <hr style="width:200px;">
    <span style="font-size:11px;">A Product By Aces Skynet Pvt Ltd.</span>
    <br/>
        <span style="font-size:11px;">The sprit of connectivity</span>
    <br/>
    <div id="login_window_1">
    <br/>
    <br/>

    <h4>Login to your account.</h4>
    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="text" style="width:100%" id="username" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-envelope"></i>&nbsp;&nbsp;Email Id</label>
    </div>

    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="password" style="width:100%" id="password" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-lock"></i>&nbsp;&nbsp;Passcode</label>
    </div>
 <button class="btn btn-sm btn-danger" style="width:50%" id="logButton" onclick="loginNow();">TAKE ME IN <i class="fa fa-arrow-right"></i></button>

</div>

    <div id="login_window_2" style="display:none">
    <br/>
    <br/>

    <h4>Please enter your mobile number for OTP verification.</h4>
    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="text" style="width:100%" id="mobile" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-envelope"></i>&nbsp;&nbsp;Mobile Number</label>
    </div>
    <span style="font-size:11px;">You will receive an OTP via sms on above mobile nnumber for login verification.</span>
    <br/>
    <br/>

 <button class="btn btn-sm btn-danger" style="width:50%" id="optbutton" onclick="sendOtp();">SEND OTP <i class="fa fa-arrow-right"></i></button>

</div>


    <div id="login_window_3" style="display:none">
    <br/>
    <br/>

    <h4>Please enter verification code received on your mobile</h4>
    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="text" style="width:100%" id="otp" required="" value=<?php echo $_session['temp_otp'];?>>
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-envelope"></i>&nbsp;&nbsp;Enter OTP</label>
    </div>


<button class="btn btn-sm btn-warning" style="width:47%" id="resendbutton" onclick="resend();"><i class="fa fa-arrow-left"></i> RESEND</button>&nbsp;&nbsp;
 <button class="btn btn-sm btn-danger" style="width:47%" id="confirmbutton" onclick="verifyOtp();">VERIFY <i class="fa fa-arrow-right"></i></button>

</div>




    </div>
  </div>
</div>


<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

<script type="text/javascript">
  hideToast();
</script>
</body>
</html>
