<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `school` ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="toast('Module not activated, as no data of school is available','toast bg-danger','3000')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Schools
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>Address</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary"><?php echo $row['name'];?></td>
<td><?php echo $row['address'];?></td>
</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>