<?php
include("../include/config.php");
$route = $_GET['route'];
$class = $_GET['class'];
if($class != '')
{
$class = explode(",",$class);
$classStr = '';
foreach($class as $val)
{
	if($val != '')
	{
		$classStr .= "student.class = ".$val." OR ";		
	}

}

$classStr = substr($classStr,0,-3);
$classStr = "(".$classStr.")";
$getStudents  = mysqli_query($con,"SELECT student.id, student.name,student.stopname FROM student WHERE ".$classStr." AND student.stopid IN (SELECT `stopid` FROM `routestopmap` WHERE `routeid` = '$route') ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="row">
	<div class="col-sm-6">
	<div style="height:400px;overflow-y:scroll">
		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
			<tr>
				<th colspan="3">Students With Matching Stops</th>
			</tr>
			<tr>
			<th>
#
			</th>
				<th>Name</th>
				<th>Stop Name</th>
			</tr>
			<?php
			$already = Array();
			$k=0;

while($row = mysqli_fetch_array($getStudents))
{
	$already[] .= $row[0];
	?>
	<tr onclick="toggleCheck('match<?php echo $k;?>')">
	<td>
		<input type="checkbox" value="<?php echo $row[0];?>" id="match<?php echo $k;?>"/>
	</td>
		<td>
			<?php echo $row[1];?>
		</td>
		<td>
			<?php echo $row[2];?>
		</td>
	</tr>
	<?php
	$k++;
}
			?>

		</table>

<input type="" id="totalMatch" value="<?php echo $k;?>" name="" style="display:none !important">
	</div>
	</div>


	<div class="col-sm-6">
	<div style="height:400px;overflow-y:scroll">
		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
			<tr>
				<th colspan="3">Remaining Students</th>
			</tr>
			<tr>
			<th>#</th>
				<th>Name</th>
				<th>Stop Name</th>
			</tr>
			<?php
			$k=0;
			$getData = mysqli_query($con,"SELECT * FROM `student` WHERE ".$classStr." ORDER BY `name` ASC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	if(!in_array($row['id'],$already))
	{

	?>
	<tr onclick="toggleCheck('unmatch<?php echo $k;?>')">
	<td>
		<input type="checkbox" value="<?php echo $row['id'];?>" id="unmatch<?php echo $k;?>"/>
	</td>
		<td>
			<?php echo $row['name'];?>
		</td>
		<td>
			<?php echo $row['stopname'];?>
		</td>
	</tr>
	<?php
	$k++;
}
}
			?>
		</table>
		<input type="" id="totalUnMatch" value="<?php echo $k;?>" name="" style="display:none !important">
		</div>
	</div>
	<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%"> 
<select class="w3-input input1bdark" type="text" style="width:100%" id="selRouteType" name="req" required="">
<option value="route">Weekdays Morning Route</option>
<option value="routereverse">Weekdays Evening Route</option>
<option value="routesat">Saturdays Morning Route</option>
<option value="routesatrev">Saturdays Evening Route</option>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route Type</label>
    </div>
	</div>

	<div class="col-sm-12">
		<br/>
		<br/>
		<button class="btn btn-primary" onclick="saveBulkRoute()">SAVE DATA</button>
		<br/>
		<br/>

	</div>
</div>

<?php
}
?>
