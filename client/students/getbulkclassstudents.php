<?php
include("../include/config.php");
$class= $_GET['class'];
$getData = mysqli_query($con,"SELECT * FROM `student` WHERE `class` = '$class' ORDER BY `name` ASC") or die(mysqli_error($con));
?>

<div class="row">
	<div class="col-sm-6">
			SELECT STUDENT TO TRANSFER TO ANOTHER CLASS

		<div style="height:400px;overflow-y:scroll">
		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
			<tr>
				<th>#</th>
				<th>Name</th>
			</tr>
			<?php
			$k=0;
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>
					<td>
						<input type="checkbox" value="<?php echo $row['id'];?>" name="" id="student<?php echo $k;?>">
					</td>
					<td onclick="toggleCheck('student<?php echo $k;?>')">
						
						<?php echo $row['name'];?>
					</td>
				</tr>
	<?php
	$k++;
}
			?>
			
			</table>
			<input type="" id="totalStudents" name="" value="<?php echo $k;?>" style="display:none !important">
		</div>
	</div>
	<div class="col-sm-6">
		<div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="blkClassTo" name="req" required="">
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>"><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Transfer To Class</label>
    </div>
    <br/>
    <br/>

<button class="btn btn-primary" onclick="shiftStudents()">
	SHIFT STUDENTS
</button>

    <br/>
	</div>
</div>