<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
$p1 = rand(100000,999999);
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;

<!--

			<button lang="changeClass" id="" class="btn btn-info btn-sm" type="button" onclick="getModal('students/messages.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')">
			<i class="fa fa-envelope"></i>&nbsp;&nbsp;MESSAGES</button>&nbsp;&nbsp;


			<button lang="changeClass" id="" class="btn btn-info btn-sm" type="button" onclick="getModal('students/diary.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')">
			<i class="fa fa-file"></i>&nbsp;&nbsp;DIARY</button>&nbsp;&nbsp;

			<button lang="changeClass" id="successdiv" class="btn btn-success btn-sm" type="button" onclick="getModule('fees/reminder.php?id=<?php echo $id;?>','successdiv','','loading')" >
			<i class="fa fa-credit-card"></i>&nbsp;&nbsp;SEND FEES REMINDER</button>&nbsp;&nbsp;

-->



			<button lang="changeClass" id="moduleSaveButtontopStudent" class="btn btn-primary btn-sm"  onclick="savedata('<?php echo $saveurl;?>','','','stop',13,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button" style="display:none">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			</div>

	<div class="moduleHeading">
		Student Details

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop0" name="req" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop1" name="req" required="" onblur="checkStudentId();">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Student Id</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
	 <select class="w3-input input1bdark" type="text" style="width:100%" id="stop2" name="req" required="">
	 <?php
	 $class = $row['class'];
$getClass = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name`") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>     
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Class</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
 <select class="w3-input input1bdark" type="text" style="width:100%" id="stop3" name="req" required="">
	 <?php
	 $school = $row['school'];
$getClass = mysqli_query($con,"SELECT * FROM `school`") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>     
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    School</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop4" name="req" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Address</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">    

<select class="w3-input input1bdark" type="text" style="width:100%" id="stop5" name="req" required="">
	 <?php
$city = $row['city'];
$getClass = mysqli_query($con,"SELECT * FROM `city`") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option <?php if($row['city'] == $rowClass['id']) echo "selected='selected'";?> value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    City</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop6" name="req" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Emergency Contact</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop7" name="req" required=""">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Father</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop8" name="req"  required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Mother</label>
    </div>
</div>

<div class="col-sm-6" >
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop9" name="req" required="" >
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Mobile</label>
    </div>
</div>



<div class="col-sm-6" >
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop10" name="req"  required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Username</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop11" name="req" required="" value="<?php echo $p1;?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Password</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop12" name="req" required="">
<option value="1">Yes</option>
<option value="0" selected="selected">No</option>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    SMS Activated</label>
    </div>
</div>



</div>
</div>



<br />
<br />
<br />
<br />
<br />
