<?php
include("../include/config.php");
$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT * FROM `diary` WHERE `student` = '$id' ORDER BY `id` DESC") or die(mysqli_error($con));

?>
<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
	<div class="moduleHeading">
	Diary
</div>
</div>
<br/>
<textarea class="input" id="heading" style="width:100%;height:50px;border:1px #eee solid;padding:10px;" placeholder="Enter Heading"></textarea>

<br/>
<br/>
<textarea class="input" id="msg" style="width:100%;height:100px;border:1px #eee solid;padding:10px;" placeholder="Enter Diary Message"></textarea>
<br/>
<br/>
<button class="btn btn-sm btn-primary" onclick="saveDiary('<?php echo $id;?>')">SAVE DIARY MESSAGE</button>
<br/>
<br/>
<br/>

<div id="msg-container">
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<div style="border-bottom:1px #eee solid;font-size:14px;">

<br/>
<strong style="color:#888 !important">
<?php echo $row['heading'];?>
</strong>
<br/>
<span style="font-size:14px;"><?php echo $row['text'];?></span>

<br/>
<span style="font-size:10px;"><?php echo date("d-m-y, h:i A",strtotime($row['createdate']));?></span>
<br/>
<br/>
</div>
	<?php
}
?>

</div>
</div>

</div>