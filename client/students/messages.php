<?php
include("../include/config.php");
$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT * FROM `messages` WHERE `student` = '$id' ORDER BY `id` DESC") or die(mysqli_error($con));

?>
<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
	<div class="moduleHeading">
	Messages
</div>
</div>
<br/>
<textarea class="input" id="msg" style="width:100%;height:100px;border:1px #eee solid;padding:10px;" placeholder="Enter New Message"></textarea>
<br/>
<br/>
<button class="btn btn-sm btn-primary" onclick="sendMessageSingle('<?php echo $id;?>')">SEND MESSAGE</button>
<br/>
<br/>
<br/>

<div id="msg-container">
<?php
while($row = mysqli_fetch_array($getData))
{
	if($row['from'] == 'S')
	{
		$disp = '<span class="label label-info">School</span>';
	}
	else
	{
		$disp = '<span class="label label-warning">Parent</span>';
	}
	?>
<div style="border-bottom:1px #eee solid;font-size:14px;">

<br/>
<?php echo $disp;?>
<br/>
<br/>
<?php echo $row['message'];?>
<br/>
<span style="font-size:10px;"><?php echo date("d-m-y, h:i A",strtotime($row['createdate']));?></span>
<br/>
<br/>
</div>
	<?php
}
?>

</div>
</div>

</div>