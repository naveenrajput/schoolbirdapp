<?php
include("../include/config.php");
$term = '';
$pages = 0;
if(isset($_GET['term']))
{
	$indata = $_GET['indata'];
	$term = $_GET['term'];
	if($indata != 'parents.father' && $indata != 'parents.mother' && $indata != 'parents.username')
	{
$getData = mysqli_query($con,"SELECT student.name,student.stid,class.name,school.name,student.address,student.emergency,student.id FROM student,class,school WHERE student.class = class.id AND student.school = school.id AND ".$indata." LIKE '%$term%' AND student.left = '1' ORDER BY student.name ASC") or die(mysqli_error($con));

	}
else
{

$getData = mysqli_query($con,"SELECT student.name,student.stid,class.name,school.name,student.address,student.emergency,student.id FROM student,class,school,parents WHERE student.parentid = parents.id AND student.class = class.id AND student.school = school.id AND ".$indata." LIKE '%$term%'  AND student.left = '1'  ORDER BY student.name ASC") or die(mysqli_error($con));

}

}
else
{
if(isset($_GET['page']))
{
	$page = $_GET['page'];
	$limitfrom = ($page - 1) * 100;
	$limit = $limitfrom.",100"; 
}
else
{
	$limit = '0,100';
}
$getData = mysqli_query($con,"SELECT student.name,student.stid,class.name,school.name,student.address,student.emergency,student.id FROM student,class,school WHERE student.class = class.id AND student.school = school.id AND student.left = '1' ORDER BY student.name   ASC LIMIT ".$limit) or die(mysqli_error($con));

$getTotal = mysqli_query($con,"SELECT student.name,student.stid,class.name,school.name,student.address,student.emergency FROM student,class,school WHERE student.class = class.id AND student.school = school.id AND student.left = '1'  ORDER BY student.name ASC");
$total = mysqli_num_rows($getTotal);
if($total % 100 == 0)
{
	$pages = $total/100;
}
else
{
	$pages = ($total/100)  + 1;
}


}
?>
<div class="moduleHead">
<div style="float:right">

&nbsp;&nbsp;&nbsp;&nbsp;
<?php
if($loggeduserid != 8)
{
?>
	<button class="btn btn-sm btn-danger"  onclick="getModule('students/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
&nbsp;&nbsp;&nbsp;&nbsp;

<div class="dropdown" style="display:inline-block">
  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">MORE ACTIONS
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li  onclick="getModule('students/bulkroute.php','formDiv','tableDiv','loading')">BULK CHANGE ROUTE</li>
    <li  onclick="getModule('students/merge-parents.php','formDiv','tableDiv','loading')">MERGE PARENTS</li>
    <li onclick="getModule('students/bulk-class-change.php','formDiv','tableDiv','loading')">BULK CLASS CHANGE</li>
  </ul>
</div>


<?php	
}
?>

</div>
<div class="moduleHeading">
Students
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<div style="padding:10px;">
	<input type="text" class="input1bdark" id="searchterm" value="<?php echo $term;?>" name="" style="width:200px;" placeholder="Search Here">&nbsp;&nbsp;
&nbsp;&nbsp;IN &nbsp;&nbsp;&nbsp;&nbsp;
<select class="input1bdark" id="indata"  style="width:200px;">
<option value="student.name" <?php if($_GET['indata'] == 'student.name') echo "selected='selected'";?> >Student Name</option>
<option value="student.stid" <?php if($_GET['indata'] == 'student.stid') echo "selected='selected'";?> >Student Id</option>
<option value="parents.father" <?php if($_GET['indata'] == 'parents.father') echo "selected='selected'";?> >Father Name</option>
<option value="parents.mother" <?php if($_GET['indata'] == 'parents.mother') echo "selected='selected'";?> >Mother Name</option>
<option value="parents.username" <?php if($_GET['indata'] == 'parents.username') echo "selected='selected'";?> >Parent Name</option>
<option value="student.address" <?php if($_GET['indata'] == 'student.address') echo "selected='selected'";?> >Address</option>
<option value="student.emergency" <?php if($_GET['indata'] == 'student.emergency') echo "selected='selected'";?> >Mobile</option>
<option value="student.stop" <?php if($_GET['indata'] == 'student.stop') echo "selected='selected'";?> >Student Stop</option>

</select>
&nbsp;&nbsp;&nbsp;

<button class="btn btn-sm btn-primary" id="thisisleftindex" onclick="searchStudents()">SEARCH NOW&nbsp;&nbsp;
<i class="fa fa-arrow-right"></i>
</button>
</div>
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>Student Id</th>
<th>Class</th>
<th>School</th>
<th>Address</th>
<th>Emergency Contact</th>
</tr>
<?php
$j = $limitfrom;
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row[6];?>"<?php if($loggeduserid!=8){?> onclick="getModule('students/edit.php?id=<?php echo $row[6];?>','formDiv','tableDiv','loading')"<?php }?>>
<td><?php echo $j+1;?></td>
<td class="text-primary"><?php echo $row[0];?></td>
<td><?php echo $row[1];?></td>
<td><?php echo $row[2];?></td>
<td><?php echo $row[3];?></td>
<td><?php echo $row[4];?></td>
<td><?php echo $row[5];?></td>
</tr>
<?php
$j++;
}
?>
</table>
</div>
<?php
?>
<ul class="pagination">


<?php

if($total > 100)
{
for($j=0;$j<$pages;$j++)
{
	?>
  <li onclick="getModule('students/leftindex.php?page=<?php echo $j+1;?>','tableDiv','formDiv','loading');"><?php echo $j+1;?></li>
	<?php
}
}
?>
</ul>
<br/><br/><br/><br/><br/><br/><br/><br/><br/>