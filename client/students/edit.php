<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$getData = mysqli_query($con, "SELECT * FROM `student` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;

<!--

			<button lang="changeClass" id="" class="btn btn-info btn-sm" type="button" onclick="getModal('students/messages.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')">
			<i class="fa fa-envelope"></i>&nbsp;&nbsp;MESSAGES</button>&nbsp;&nbsp;


			<button lang="changeClass" id="" class="btn btn-info btn-sm" type="button" onclick="getModal('students/diary.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')">
			<i class="fa fa-file"></i>&nbsp;&nbsp;DIARY</button>&nbsp;&nbsp;

			<button lang="changeClass" id="successdiv" class="btn btn-success btn-sm" type="button" onclick="getModule('fees/reminder.php?id=<?php echo $id;?>','successdiv','','loading')" >
			<i class="fa fa-credit-card"></i>&nbsp;&nbsp;SEND FEES REMINDER</button>&nbsp;&nbsp;

-->

<?php
if($row['left'] == '0')
{

?>    
			<button lang="changeClass" id="successdiv1" class="btn btn-success btn-sm" type="button" onclick="getModule('students/sendlogin.php?id=<?php echo $id;?>&mobile='+document.getElementById('stop9').value,'successdiv1','','loading')" >
			<i class="fa fa-envelope"></i>&nbsp;&nbsp;SEND LOGIN DETAILS</button>&nbsp;&nbsp;
			

			<button lang="changeClass" id="successdiv2" class="btn btn-danger btn-sm" type="button" onclick="getModule('students/markleft.php?id=<?php echo $id;?>','successdiv2','','loading')" >
			<i class="fa fa-close"></i>&nbsp;&nbsp;MARK AS LEFT</button>&nbsp;&nbsp;
			<?php
			
			
}

?>

			
<?php
if($loggeduserid != 8 || $loggeduserid != 9)
{
  ?>
      <button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm"  onclick="savedata('<?php echo $updateurl;?>','','','stop',18,'','','tableDiv','formDiv');" type="button">
      <i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>


  <?php
}
?>


			</div>

	<div class="moduleHeading">
		Student Details

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop0" name="req" required="" value="<?php echo $row['name'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop1" name="req" required="" readonly="readonly" value="<?php echo $row['stid'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Student Id</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
	 <select class="w3-input input1bdark" type="text" style="width:100%" id="stop2" name="req" required="">
	 <?php
	 $class = $row['class'];
$getClass = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name`") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option <?php if($row['class'] == $rowClass['id']) echo "selected='selected'";?> value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>     
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Class</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
 <select class="w3-input input1bdark" type="text" style="width:100%" id="stop3" name="req" required="">
	 <?php
	 $school = $row['school'];
$getClass = mysqli_query($con,"SELECT * FROM `school` WHERE `id` = '$school'") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>     
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    School</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop4" name="req" required="" value="<?php echo $row['address'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Address</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">    

<select class="w3-input input1bdark" type="text" style="width:100%" id="stop5" name="req" required="">
	 <?php
	 $city = $row['city'];
$getClass = mysqli_query($con,"SELECT * FROM `city`") or die(mysqli_error($con));
while($rowClass = mysqli_fetch_array($getClass))
{
	?>
<option <?php if($rowClass['id'] == $city) echo "selected='selected'";?> value="<?php echo $rowClass['id'];?>"><?php echo $rowClass['name'];?></option>
	<?php
}
	 ?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    City</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop6" name="req" required="" value="<?php echo $row['emergency'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Emergency Contact</label>
    </div>
</div>

<?php
$parent = $row['parentid'];
$getParent = mysqli_query($con,"SELECT * FROM `parents` WHERE `id` = '$parent'") or die(mysqli_error($con));
$rowParent = mysqli_fetch_array($getParent);
?>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop7" name="req" required="" value="<?php echo $rowParent['father'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Father</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop8" name="req"  required="" value="<?php echo $rowParent['mother'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Mother</label>
    </div>
</div>

<div class="col-sm-6" >
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop9" name="req"  value="<?php echo $rowParent['mobile'];?>"  required="" >
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Mobile</label>
    </div>
</div>



<div class="col-sm-6" >
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop10" name="req"  value="<?php echo $rowParent['username'];?>"  required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Username</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop11" name="req"  value="<?php echo $rowParent['password'];?>"  required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Password</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop12" name="req" required="">
<option value="1" <?php if($row['sms'] == '1') echo "selected='selected'";?>>Yes</option>
<option value="0" <?php if($row['sms'] == '0') echo "selected='selected'";?>>No</option>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    SMS Activated</label>
    </div>
</div>





<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `stops` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop13" name="req" required="">
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>" <?php if($row['stopid'] == $rowStops['id']) echo "selected='selected'";?>><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Stop</label>
    </div>
</div>



<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop14" name="req" required="">
<option value="0">No Route</option>
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>" <?php if($row['route'] == $rowStops['id']) echo "selected='selected'";?>><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Morning Route Weekdays</label>
    </div>
</div>




<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop15" name="req" required="">
<option value="0">No Route</option>
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>" <?php if($row['routereverse'] == $rowStops['id']) echo "selected='selected'";?>><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Evening Route Weekdays</label>
    </div>
</div>






<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop16" name="req" required="">
<option value="0">No Route</option>
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>" <?php if($row['routesat'] == $rowStops['id']) echo "selected='selected'";?>><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Morning Route Saturdays</label>
    </div>
</div>


<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%"> 
<?php
$getStops = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
?>
<select class="w3-input input1bdark" type="text" style="width:100%" id="stop17" name="req" required="">
<option value="0">No Route</option>
<?php
while($rowStops = mysqli_fetch_array($getStops))
{
?>
<option value="<?php echo $rowStops['id'];?>" <?php if($row['routerevsat'] == $rowStops['id']) echo "selected='selected'";?>><?php echo $rowStops['name'];?></option>
<?php	
}

?>
	 </select>  

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Evening Route Saturdays</label>
    </div>
</div>

</div>
</div>



<br />
<br />
<br />
<br />
<br />
