<?php
include("../include/config.php");
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('buses/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Messages
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto;padding:10px;">
<div class="row">

<div class="col-sm-6" style="padding:0px ;">
<div class="w3-group margin10" style="width:100%">  
		<select class="w3-input input1bdark" style="width:100%;height:47px" id="class" name="bus" required=""  name="req" onchange="getStudents();">
		<option value="0">Select Class</option>
		<?php
		$getData = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name` ASC") or die(mysqli_error($con));
		while($row = mysqli_fetch_array($getData))
		{
			?>
			<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
			<?php			
		}
		?>
		</select>  
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Select Class</label>
		</div>
</div>
<div class="col-sm-6">
	<div class="w3-group margin10" style="width:100%" id="stucontainer">      
		<select class="w3-input input1bdark" style="width:100%;height:47px" id="student" name="bus" required=""  name="req">
		<option value="0">Select Student</option>		
		</select>  
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Select Student</label>


		</div>
</div>
<div class="col-sm-12">
	<div class="w3-group margin10" style="width:100%">      
		<textarea  class="w3-input input1bdark" style="width:100%;height:100px" id="message" name="bus" required=""  name="req" placeholder='Enter Message'></textarea>	

		</div>
</div>

<div class="col-sm-6">
<button class="btn btn-primary" type="button" onclick="sendMessage();">SEND MESSAGE&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
</div>
	</div>
</div>
<br/>
<br/>
<div id="resultholder"></div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>