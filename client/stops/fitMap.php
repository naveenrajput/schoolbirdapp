<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::PARENT EYE CLIENT ACCOUNT:.</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="../css/map.css"/>
<script type="text/javascript" src="../scripts/maps.js"></script>
<style type="text/css">
	body
	{
		padding: 0px;
		margin: 0px;
		font-family: 'Rubik';
	}
</style>
<?php
if(isset($_GET['lat']))
{
	?>
	<script type="text/javascript">
		myLat = <?php echo $_GET['lat'];?>;
		myLang = <?php echo $_GET['lng'];?>;
		preLat = '1';
	</script>
	<?php
}
?>
</head>
<body>
	

	<input id="pac-input" class="controls" type="text" placeholder="Search Your Stop Here">
	<div style="width:100%;height:550px" id="map"></div>

	<div class="toast" id="toast"></div>
</body>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initAutocomplete"></script>
</html>
