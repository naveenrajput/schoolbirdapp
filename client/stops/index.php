<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `stops` ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
    <?php if($loggeduserid!=8){?>
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('stops/new.php','formDiv','tableDiv','loading')">+1 ADD NEW STOP</button>&nbsp;&nbsp;
	<button class="btn btn-sm btn-danger"  onclick="window.open('stops/bulk-map.php')">ADD BULK STOPS</button>

</div>
<?php }?>
<div class="moduleHeading">
Stops
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>Address</th>
<th>View on Map</th>
<th>Createdate</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>" onclick="getModule('stops/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')">
<td><?php echo $j+1;?></td>
<td class="text-primary"><?php echo $row['name'];?></td>
<td><?php echo $row['address'];?></td>
<td>
	<span class="label label-primary"><i class="fa fa-map-marker"></i> VIEW ON MAP</span>
</td>
<td><?php echo date("d-m-y",strtotime($row['createdate']));?></td>
</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>