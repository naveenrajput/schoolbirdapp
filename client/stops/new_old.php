<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
?>
<div class="moduleHead">
		<div style="float: right">
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','build',2,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Save</button>
			<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;
			Cancel</button></div>

	<div class="moduleHeading">
		Add A New Payment Mode

	</div>
</div>
<div class="row">
	<div style="padding: 5px 28px;">
		<div class="divShadow">
			<div class="col-sm-12 formHead">
				Details</div>
			<div class="col-sm-12 formCol" style="padding-top: 0px;">
				<div class="row">
										<div class="col-sm-2 divLeft">
					Name</div>
					<div class="col-sm-10 divRight">
						<input id="build0" class="input" style="width: 93%" type="text" name="req" title="Name" />
					</div>
				</div>

<div class="row">
					
										<div class="col-sm-2 divLeft"  style="height:200px !important;">
						Notes</div>
					<div class="col-sm-10 divRight" style="height:200px !important;">
					<textarea class="input" style="height:150px;width:95%" id="build1" type="text"></textarea>
					</div>
				</div>

<div class="row">
					
										<div class="col-sm-2 divLeft"  style="height:200px !important;">
						Is Cheque</div>
					<div class="col-sm-10 divRight" style="height:200px !important;">
					<select class="input" id="build2" style="width:95%">
						<option value="0">No</option>
						<option value="1">Yes</option>
					</select>
					</div>
				</div>				
			</div>
			<br />
			<br />
			
			
						<div class="col-sm-12 formCol" style="padding-top: 0px;">
					<div class="row">
					<div class="col-sm-2" style="height: 60px !important;">
						</div>
					<div class="col-sm-10 divRight" style="height: 60px !important;">
			<button lang="changeClass" id="moduleSaveButton" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','build',3,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Save</button>
			<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;
			Cancel</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
