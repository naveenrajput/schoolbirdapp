<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::PARENT EYE CLIENT ACCOUNT:.</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="../css/map.css"/>
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<script type="text/javascript" src="../scripts/bulk-stop.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>

<style type="text/css">
	body
	{
		padding: 0px;
		margin: 0px;
		font-family: 'Rubik';
	}
</style>
</head>
<body>

<div style="position:fixed;top:0;right:0;width:20%;;z-index:2000;background:#fff;" id="topCoverDiv">
<div style="height:600px;overflow-y:scroll" id="stopList">
</div>
	<div style="position:fixed;bottom:0px;width:20%;height:100px;padding:20px;text-align:center;background:rgba(255,255,255,0.6);border-top:1px #eee solid;">
		<button class="btn btn-primary" onclick="collectValues();" id="saveStop">SAVE STOPS</button>

		<button class="btn btn-danger" onclick="resetStops();" id="resetStops">RESET</button>
		
	</div>
</div>
	

	<input id="pac-input" class="controls" type="text" placeholder="Search Your Stop Here">
	<div style="width:80%;height:700px;" id="map"></div>

	
<div class="toast" id="toast" style="bottom:-300px">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>
</body>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initAutocomplete"></script>

<script type="text/javascript">
var inHeight = window.innerHeight;
	$('#topCoverDiv').animate({height:inHeight});
var lowerHeight = window.innerHeight - 100;
	$('#stopList').animate({height:lowerHeight});
</script>
</html>
