<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$table = "stops";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;
<?php
if($loggeduserid == 2)
{
?>
			<button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $row['id'];?>','stops');" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			DELETE ENTRY</button>&nbsp;&nbsp;

<?php
}
?><?php
if($loggeduserid == 8 || $loggeduserid == 9 )
{}else{
?>
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','stop',5,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>

<?php
}
?>
			</div>

	<div class="moduleHeading">
		Stop Details

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop0" name="req" required="" value="<?php echo $row['name'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop1" name="req" required="" value="<?php echo $row['address'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Address</label>
    </div>
</div>


<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop4" name="req" required="" value="<?php echo $row['identifier'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Identifier (For future reference to this stop)</label>
    </div>
</div>

<div class="col-sm-6" onclick="toast('Please drop a pin on map to select Latitude','toast bg-danger','0')">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop2" name="req"  required="" lang="<?php echo $row['lat'];?>" onkeyup="this.value = this.lang"  value="<?php echo $row['lat'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Latitude</label>
    </div>
</div>

<div class="col-sm-6"  onclick="toast('Please drop a pin on map to select Longitude','toast bg-danger','2000')">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" id="stop3" name="req"  lang="<?php echo $row['lng'];?>"  required="" onkeyup="this.value = this.lang" value="<?php echo $row['lng'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Longitude</label>
    </div>
</div>

<div id="col-sm-12">

<div style="border:10px #fff solid;border-radiius:5px;" id="infoMap">
<br/>
	Please click on the right spot on the map for this location.
	<br/>
<iframe src="stops/fitMap.php?lat=<?php echo $row['lat'];?>&lng=<?php echo $row['lng'];?>" style="height:600px;width:100%;" frameborder="0" scrolling="no"></iframe>
</div>

</div>


</div>
</div>



<br />
<br />
<br />
<br />
<br />
