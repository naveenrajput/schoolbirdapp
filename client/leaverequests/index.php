<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT student.name,class.name,leaverequest.fromdate,leaverequest.todate,leaverequest.remark,leaverequest.status FROM student,class,leaverequest WHERE student.id = leaverequest.student AND student.class = class.id ORDER BY leaverequest.createdate ASC") or die(mysqli_error($con));

?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('buses/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Leave Requests
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto;padding:10px;">
<div class="row">

    <table class="table table-striped table-hovered fetch">
        <tr>
            <th>#</th><th>Name</th><th>Class</th><th>From Date</th><td>To Date</td><td>Remark</td><td>Status</td>
        </tr>
<?php
$j=1;
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>
		<td><?php echo $j;?></td>
		<td><?php echo $row[0];?></td>
		<td><?php echo $row[1];?></td>
		<td><?php echo $row[2];?></td>
		<td><?php echo $row[3];?></td>
		<td><?php echo $row[4];?></td>
		<td><?php 
		if($row[5] == 0)
		{
			?>
			<button class="btn btn-sm btn-danger">REJECT</button>&nbsp;&nbsp;
			<button class="btn btn-sm btn-success">APPROVE</button>
			<?php
		}
		else if($row[5] == 1)
		{
			?>
			<button class="btn btn-sm btn-success">APPROVED</button>
			<?php

		}

		else if($row[5] == 2)
		{
			?>
			<button class="btn btn-sm btn-danger">REJECTED</button>
			<?php

		}

		?></td>
	</tr>
	<?php
	$j++;
}
?>
</table>
</div>
<br/>
<br/>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>