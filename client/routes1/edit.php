<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$table = "routes";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);


$allotedStops = Array();
$getAllotedStops = mysqli_query($con, "SELECT * FROM `routestopmap` WHERE `routeid` = '$id'") or die(mysqli_error($con));
while($rowAlStop = mysqli_fetch_array($getAllotedStops))
{
	if($rowAlStop['type'] == 'start')
	{
		$startpoint = $rowAlStop['stopid'];
	}
	else if($rowAlStop['type'] == 'end')
	{
		$endpoint = $rowAlStop['stopid'];
	}
	else
	{
		$allotedStops[] .= $rowAlStop['stopid'];		
	}


}


$stops = Array();
$lat = Array();
$lngs = Array();
$ids = Array();


$getStops = mysqli_query($con,"SELECT * FROM `stops` ORDER BY `name` ASC") or die(mysqli_error($con));
while($rowStops =mysqli_fetch_array($getStops))
{
	$stops[] .= $rowStops['name'];
	$lat[] .= $rowStops['lat'];
	$lngs[] .= $rowStops['lng'];
	$ids[] .= $rowStops['id'];
}
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>
			<button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $row['id'];?>','routes');" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			DELETE ENTRY</button>&nbsp;&nbsp;
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','route',5,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE DATA</button>
			</div>

	<div class="moduleHeading">
		Route Details

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-2" style="border-right:1px #eee solid;">
		<div class="w3-group margin10" style="width:100%">      
		      <input class="w3-input input1bdark" type="text" style="width:100%" id="route0" name="req" required="" value="<?php echo $row['name'];?>">
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Name</label>
		</div>
<br/>
		<div class="w3-group margin10" style="width:100%">  
		<select class="w3-input input1bdark" style="width:100%;" id="route1" name="req" required="" onchange="if(this.value != '0') {displayRoute();}" name="req">
		<option value="0">Select Start Stop</option>
		<?php
		foreach($stops as $key => $val)
		{
			?>
			<option <?php if($startpoint == $ids[$key]) {echo "selected='selected'"; } ?> value="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>"><?php echo $val;?></option>
			<?php			
		}
		?>
		</select>  
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     First Stop</label>
		</div>

<br/>
		<div class="w3-group margin10" style="width:100%">  
		<select class="w3-input input1bdark" style="width:100%;" id="route2" name="req" required="" onchange="if(this.value != '0') {displayRoute(); }" name="req">
		<option value="0">Select Final Stop</option>
		<?php
		foreach($stops as $key => $val)
		{
			?>
			<option <?php if($endpoint == $ids[$key]) {echo "selected='selected'"; } ?>  value="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>"><?php echo $val;?></option>
			<?php			
		}
		?>
		</select>  
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Last Stop</label>
		</div>


<br/>
<!--
		<div class="w3-group margin10" style="width:100%">  

	<ul class="list-group" style="cursor:pointer;margin-top:10px !important;">
		<?php
		$k=0;
		$tleng = count($stops);
		foreach($stops as $key => $val)
		{
			if(in_array($ids[$key], $allotedStops)) 
			{
				$thisClass = 'list-group-item li-selected';
			}
			else
			{
				$thisClass = 'list-group-item';
			}
			?>
  <li id="li<?php echo $k;?>" onclick="collectStopValues('<?php echo $k;?>','<?php echo $tleng;?>','route4')" lang="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>" class="<?php echo $thisClass;?>"><?php echo $val;?></li>
			<?php
			$k++;
}
			?>


</ul>


		<select class="w3-input input1bdark" style="width:100%;border:1px #eee solid !important;display:none" id="route3" name="req"multiple="multiple" size="8" onchange="displayRoute();multiSelect('route3','route4')">
		<?php
		foreach($stops as $key => $val)
		{
			?>
			<option <?php if(in_array($ids[$key], $allotedStops)) {echo "selected='selected'"; } ?> value="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>"><?php echo $val;?></option>
			<?php			
		}
		?>
		</select>  
		<input type="" id="route4" name="" style="display:none" name="req" value="<?php echo implode(",",$allotedStops);?>">
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Way Points (Press Crtl+Mouse Click for multiple selection)</label>
		</div>
-->

<br/><br/>
</div>




<div class="col-sm-3">
	<div class="w3-group margin10" style="width:100%;">  
 <span class="" style="font-size:11px !important;color:#999">
		     Way Points (Click to select/deselect)</span>

<br/>
<div style="padding:10px;">

</div>

 <div class="list-group-item filterTabLi">
  	<br/>
	<input type="" class="input1bdark" name="" placeholder="Search Here" style="width:95%;" onkeyup="filterRouteSelect('')" id="stopSearcher">
	<br/>
	<br/>
	<table style="width:100%" class="filterTabs">
		<tr>
			<td class="active" onclick="filterRouteSelect('all')" id="allTab">ALL</td>
			<td onclick="filterRouteSelect('selected')" id="selectedTab">SELECTED</td>
			<td onclick="filterRouteSelect('not-selected')" id="not-selectedTab">NOT-SELECTED</td>
		</tr>
	</table>

  </div>
<ul class="list-group" style="cursor:pointer;margin-top:0px !important;height:500px;overflow-y:auto;;overflow-x:hidden">
		
 

		<?php
		$k=0;
		$tleng = count($stops);
		foreach($stops as $key => $val)
		{

			if(in_array($ids[$key], $allotedStops)) 
			{
				$thisClass = 'list-group-item li-selected';
			}
			else
			{
				$thisClass = 'list-group-item';
			}

			?>
  <li id="li<?php echo $k;?>" onclick="collectStopValues('<?php echo $k;?>','<?php echo $tleng;?>','route4')" lang="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>" title="<?php echo $val;?> , <?php echo $addresses[$key];?> ,  <?php echo $identifiers[$key];?>" class="<?php echo $thisClass;?>">

<div style="float:right">
<?php
if( $identifiers[$key] != '')
{
	?>
	<span class="label label-danger">
<?php echo $identifiers[$key];?>
	</span>

	<?php
}
?>
</div>
  <?php echo $val;?>
  	<br/>
  	<span style="font-size:10px;"><?php echo $addresses[$key];?></span>
  </li>
			<?php
			$k++;
}
			?>


</ul>

<input type="" id="totalStops" value="<?php echo $tleng;?>" name="" style="display:none">
<select class="w3-input input1bdark" style="width:100%;border:1px #eee solid !important;display:none" id="route3" name="req"multiple="multiple" size="8" onchange="displayRoute();multiSelect('route3','route4')">
		<?php
		foreach($stops as $key => $val)
		{
			?>
			<option  <?php if(in_array($ids[$key], $allotedStops)) {echo "selected='selected'"; } ?>  value="<?php echo $lat[$key];?>,<?php echo $lngs[$key];?>:::<?php echo $ids[$key];?>:::<?php echo $val;?>"><?php echo $val;?></option>
			<?php			
		}
		?>
		</select> 

		<input type="" id="route4" name="" style="display:none" name="req" value="<?php echo implode(",",$allotedStops);?>">
		     
		</div>
</div>




<div class="col-sm-7 shadow" style="padding:0px">
<div class="row">
	<div class="col-sm-4"  style="padding:10px;position:relative">
<div style="position:absolute;top:0;left:30px;width:1px;height:600px;background:#ddd;display:none;" id="routeLine"></div>


		 <div id="directions-panel" style="width:100%;height:580px;overflow-y:auto">
		Routes according to selected stops will appear on Map and the list will appear here.

	  </div>
	</div>

<div class="col-sm-8"  style="padding:0px">
<div style="position:absolute;top:10px;right:10px;">
	<button class="btn btn-sm btn-info">Total Distance: <span id="totalDistance">0</span> Kms</button>
</div>
<iframe src="routes/displayRoute.php?pre=1" id="targetFrame" frameborder="0" scrolling="no" style="width:100%;height:600px;"></iframe>
</div>
</div>
</div>






</div>
</div>



<br />
<br />
<br />
<br />
<br />

