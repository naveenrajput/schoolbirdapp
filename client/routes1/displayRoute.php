<!DOCTYPE html>
<html>
  <head>
  <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in directions</title>
    <style>
      #right-panel {
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }

      #right-panel select, #right-panel input {
        font-size: 15px;
      }

      #right-panel select {
        width: 100%;
      }

      #right-panel i {
        font-size: 12px;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        font-family: 'Rubik'
      }
      #map {

  
        width: 100%;
        height: 600px;
      }
      #right-panel {
        margin: 20px;
        border-width: 2px;
        width: 20%;
        height: 400px;
        float: left;
        text-align: left;
        padding-top: 0;
      }
      .directions-panel-outer {
        position: fixed;
        background: #fff;
        -webkit-box-shadow: 0px 10px 25px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 0px 10px 25px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 10px 25px 0px rgba(0,0,0,0.1);
    top:0px;
    left:0px;
    width:100%;
  padding: 10px;     
  z-index: 20000   
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
  
    <script>

var preload = '0';
<?php
if(isset($_GET['pre']))
{
  ?>
preload = '1';
  <?php
}
?>

    var myLat = localStorage.getItem("myLat");
myLat = parseFloat(myLat);

var myLang = localStorage.getItem("myLang");
myLang = parseFloat(myLang);

var directionsService;
var directionsDisplay;
var html;
var org;
var dest;
var marker = [];
var map;
var infowindow = [];
var totalDistance = 0;
function generateRoute()
{
  calculateAndDisplayRoute(directionsService, directionsDisplay);
}

      function initMap() {
         directionsService = new google.maps.DirectionsService;
         //directionsDisplay = new google.maps.DirectionsRenderer;
directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    });
         map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: {lat: myLat, lng: myLang}
        });
        directionsDisplay.setMap(map);


if(preload == '1')
{
  generateRoute();
}
/*
        document.getElementById('submit').addEventListener('click', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
        */
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
        /*
        var checkboxArray = window.top.window.document.getElementById('route3');
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {

             var temp = checkboxArray[i].value;
             temp = temp.split(":::");

          
          }
        }*/
var total = window.top.window.document.getElementById('totalStops').value;
for(j=0;j<total;j++)
{
  if(window.top.window.document.getElementById('li'+j))
  {
    if(window.top.window.document.getElementById('li'+j).className == 'list-group-item li-selected')
    {
      var x = window.top.window.document.getElementById('li'+j).lang;
      x = x.split(":::");
      waypts.push({
              location: x[0],
              stopover: true
            });
    }
  }
}

        org = window.top.window.document.getElementById('route1').value;
        org = org.split(":::");

        dest = window.top.window.document.getElementById('route2').value;
        dest = dest.split(":::");


        directionsService.route({
          origin: org[0],
          destination: dest[0],
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = window.top.window.document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            html = '';
            totalDistance = 0;
            for (var i = 0; i < route.legs.length; i++) {
              var thisLab = i+1;



              var routeSegment = i + 1;

       html += '<div style="position:relative;width:100%;">';
    if(i == 0)
      {
      html += '<img src="images/pin-dir.png" style="position:absolute;top:0px;left:10px;width:20px;"alt=""/><div style="padding-left: 40px;min-height:40px">';
     html += route.legs[i].start_address; 
      html += '</div>';
      }

      html += '<div style="position:relative;width:100%;"><span class="label label-success" style="position:absolute;top:10px;left:0px">';
      html += route.legs[i].distance.text;
      html += '</span></div><br/><br/><div style="position:relative;width:100%;padding-left:40px"><img src="images/pin-dir.png" style="position:absolute;top:0px;left:10px;width:20px;"alt=""/></div><div style="padding-left: 40px;;min-height:40px">';
       html += route.legs[i].end_address ;   
      html += '</div></div>';
      tdest = parseFloat(route.legs[i].distance.text);
      tdest = Math.round( tdest * 10 ) / 10;
totalDistance = tdest+totalDistance;




            }


                 summaryPanel.innerHTML = html;
              window.top.window.document.getElementById('routeLine').style.display= 'block';
              totalDistance = Math.round( totalDistance * 10 ) / 10;
              window.top.window.document.getElementById('totalDistance').innerHTML = totalDistance;

addMarkers();
          } else {
            window.top.window.toast('Please select appropriate values in all fields to get valid results on map.','toast bg-warning','4000');
          }
        });
      }

var thisMarkerPos = [];
var upwypts = [];
function addMarkers()
{
for(k=0;k<upwypts.length;k++)
{
  if(marker[k])
  {
    marker[k].setMap(null);
  }
}

var from = window.top.window.document.getElementById('route1').value;
from = from.split(":::");
var to = window.top.window.document.getElementById('route2').value;
to = to.split(":::");
upwypts = [];

var total = window.top.window.document.getElementById('totalStops').value;
for(j=0;j<total;j++)
{
  if(window.top.window.document.getElementById('li'+j))
  {
    if(window.top.window.document.getElementById('li'+j).className == 'list-group-item li-selected')
    {
      var x = window.top.window.document.getElementById('li'+j).lang;
      x = x.split(":::");
      upwypts[j] = x[0]+"::"+x[2];
    }
  }
}




upwypts[j+1]= from[0]+"::"+from[2];
upwypts[j+2]= to[0]+"::"+to[2];


for(i=0;i<upwypts.length;i++)
{
if(upwypts[i])
{
  var temp = upwypts[i].split("::");
  var temp1 = temp[0].split(",");
var thisMarkerPos = new google.maps.LatLng(temp1[0],temp1[1]);
 marker[i] = new google.maps.Marker({
                    position: thisMarkerPos,
                    map: map,
                    icon:'../images/pin-dir.png',
                    myi:i
                });



google.maps.event.addListener(marker[i], 'click', function() {
  infowindow[this.myi].open(map,marker[this.myi]);
  });


 infowindow[i] = new google.maps.InfoWindow({
    content: '<span style="font-size:13px;color:#1d1851;font-weight:bold;">'+temp[1]+'</span>'
  });

}


}

}

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&callback=initMap">
    </script>
  </body>
</html>