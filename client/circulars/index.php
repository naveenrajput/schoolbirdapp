<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `circulars` ORDER BY `createdate` DESC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('circulars/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Circulars
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Title</th>
<th>Message</th>
<th>Class</th>
<th>Date Posted</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary"><?php echo $row['title'];?></td>
<td><?php echo $row['message'];?></td>
<td><?php
if($row['class'] == 'ALL')
{
	echo "All Classes";
}
else
{
	echo $row['class'];
}
?></td>
<td><?php echo date("d-m-y",strtotime($row['createdate']));?></td>
</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>