<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('buses/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Buses
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Bus No.</th>
<th>Driver</th>
<th>Mobile</th>
<th>Locate</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary" onclick="getModule('buses/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['bus'];?></td>
<td><?php echo $row['drivername'];?></td>
<td><?php echo $row['drivernumber'];?></td>
<td onclick="window.open('tracking/trackMap.php?buslist=<?php echo $row['id'];?>,','_blank')">
	<span class="label label-primary"><i class="fa fa-map-marker"></i> VIEW ON MAP</span>
</td>

</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>