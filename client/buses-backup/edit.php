<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$table = "buses";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;
		<button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $row['id'];?>','buses');" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			DELETE ENTRY</button>&nbsp;&nbsp;

			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-info btn-sm" onclick="getModal('buses/specialroutes.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')" type="button">
			<i class="fa fa-calendar"></i>&nbsp;&nbsp;SPECIAL ROUTES</button>&nbsp;&nbsp;

			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','bus',41,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE RECORD</button>

			
			</div>

	<div class="moduleHeading">
	Bus Details

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus0" value="<?php echo $row['bus'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Bus No.</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus1" value="<?php echo $row['drivername'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus2" value="<?php echo $row['drivernumber'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Mobile
</div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus3" value="<?php echo $row['conductorname'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Name</label>
    </div>
</div>
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus4" value="<?php echo $row['conductornumber'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Mobile</label>
    </div>
</div>

<div class="col-sm-12">
	<table class="table table-striped table-bordered" style="border:1px #eee solid">
	<tr>
		<th style="width:10%">Day/Date</th>
		<th style="width:20%">Morning Route</th>
		<th colspan="2" style="width:20%">Morning Time</th>
		<th style="width:20%">Afternoon Route</th>
		<th colspan="2" style="width:20%">Afternoon Time</th>
	</tr>
		<?php
		$routedata = Array();
		$getAllotedRoutes = mysqli_query($con,"SELECT * FROM `busroutemap` WHERE `busid` = '$id' AND `date` = '0000-00-00'") or die(mysqli_error($con));
		while($rowAlloted = mysqli_fetch_array($getAllotedRoutes))
		{
			if($rowAlloted['day'] != '')
			{
			$routedata[$rowAlloted['day']]['route'] = $rowAlloted['routeid'];
			$routedata[$rowAlloted['day']]['routeeve'] = $rowAlloted['routeeveid']; 				
			$routedata[$rowAlloted['day']]['morfrom'] = $rowAlloted['morningfrom'];
			$routedata[$rowAlloted['day']]['morto'] = $rowAlloted['morningto'];
			$routedata[$rowAlloted['day']]['evefrom'] = $rowAlloted['eveningfrom'];
			$routedata[$rowAlloted['day']]['eveto'] = $rowAlloted['eveningto'];

			}


		} 
$text = Array();
$text[1] = 'MON';
$text[2] = 'TUE';
$text[3] = 'WED';
$text[4] = 'THU';
$text[5] = 'FRI';
$text[6] = 'SAT';

$color = Array();
$color[1] = '31BEDB';
$color[2] = '47C9AD';
$color[3] = 'FECE33';
$color[4] = '239F86';
$color[5] = 'FDA529';
$color[6] = '3081B7';

$k=4;

for($l=1;$l<=6;$l++)
{
	$k = $k+1;;
	
	?>
<tr>


			<td>
				
				<div class="circleBig" style="background:#<?php echo $color[$l];?>;">
					<?php echo $text[$l];?>
				</div>

			</td>

<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus<?php echo $k;?>">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option <?php if($routedata[$text[$l]]['route'] == $routeIds[$key]) echo "selected='selected'";?> value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['morfrom'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['morto'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus<?php $k= $k+1; echo $k;?>">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option <?php if($routedata[$text[$l]]['routeeve'] == $routeIds[$key]) echo "selected='selected'";?> value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>

			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['evefrom'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['eveto'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>



			</tr>
	<?php
}
		?>

		
			
		

	</table>
</div>


</div>
</div>



<br />
<br />
<br />
<br />
<br />
