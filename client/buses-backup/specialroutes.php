<?php
include("../include/config.php");
$id = $_GET['id'];
$table = "buses";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>
<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
		<div style="float: right">
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('buses/savespecial.php?id=<?php echo $id;?>','','','busmore',36,'','routeSpecialSaved','tableModalBig','formModalBig');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
&nbsp;&nbsp;
		<button class="btn btn-danger btn-sm" onclick="$('#myModalBig').modal('hide')" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			CLOSE</button>

			
			</div>

	<div class="moduleHeading">
	Special Routes For This Bus

	</div>
</div>
<div id="routeSpecialSaved" class="btn btn-sm btn-success" style="display:none"></div>
<table class="table table-striped table-bordered" style="border:1px #eee solid">
	<tr>
		<th style="width:10%">Day/Date</th>
		<th style="width:30%">Morning Route</th>
		<th colspan="2" style="width:15%">Morning Time</th>
		<th style="width:30%">Afternoon Route</th>
		<th colspan="2" style="width:15%">Afternoon Time</th>
	</tr>
		<?php
		$routedata = Array();
		$text = Array();
		$getAllotedRoutes = mysqli_query($con,"SELECT * FROM `busroutemap` WHERE `busid` = '$id' AND `day` = ''") or die(mysqli_error($con));
		while($rowAlloted = mysqli_fetch_array($getAllotedRoutes))
		{
			$text[] .= $rowAlloted['date'];
			$routedata[$rowAlloted['date']]['route'] = $rowAlloted['routeid'];
			$routedata[$rowAlloted['date']]['routeeve'] = $rowAlloted['routeeveid']; 				
			$routedata[$rowAlloted['date']]['morfrom'] = $rowAlloted['morningfrom'];
			$routedata[$rowAlloted['date']]['morto'] = $rowAlloted['morningto'];
			$routedata[$rowAlloted['date']]['evefrom'] = $rowAlloted['eveningfrom'];
			$routedata[$rowAlloted['date']]['eveto'] = $rowAlloted['eveningto'];
		} 


$k=0;

for($l=0;$l<=5;$l++)
{
	
	?>
<tr>

<td>
				
			 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="date" value="<?php echo $text[$l];?>"  required="" id="busmore<?php echo $k;?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Date</label>
    </div>
			</td>

<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="busmore<?php $k = $k+1; echo $k;?>">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option <?php if($routedata[$text[$l]]['route'] == $routeIds[$key]) echo "selected='selected'";?> value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="busmore<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['morfrom'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="busmore<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['morto'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="busmore<?php $k= $k+1; echo $k;?>">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option <?php if($routedata[$text[$l]]['routeeve'] == $routeIds[$key]) echo "selected='selected'";?> value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select</label>
    </div>
			</td>

			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="busmore<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['evefrom'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="busmore<?php $k = $k+1;echo $k;?>" value="<?php echo $routedata[$text[$l]]['eveto'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>



			</tr>
	<?php
}
		?>

		
			
		

	</table>

</div>

</div>