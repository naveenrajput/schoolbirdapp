<?php
include("include/config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::SCHOOLBIRD CLIENT LOGIN::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/map.css"/>
<link rel="stylesheet" type="text/css" href="css/ripple.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>
<script type="text/javascript" src="scripts/location.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>  
<script type="text/javascript" src="scripts/savedata.js"></script>  

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
</head>
<body  onhashchange="changeHashValue()" onload="changeHashValue();getFirstLocation();">
<?php
include('modal.php');
?>
<div class="loading" id="loading">

<div style="position:fixed;bottom:20px;right:20px;display:none" id="stopButton">
  <button class="btn btn-primary" onclick="$('#loading').hide();">This is taking longer than expected. (Click here to stop request).</button>
</div>

<div style="width:100%;display:inline-block;text-align:left">
 <div style="width:100%;height:7px;background:#222;text-align:left;">
    <div style="width:30%;height:7px;background:#b93d3c;display:inline-block" id="loadbar"></div>
  </div>
<!--
<div style="float:right" id="loadperc">
<i class="fa fa-circle-o-notch fa-spin" style="font-size:11px;color:#000"></i>
</div>
processing..
-->
 

</div>


</div>
<div class="row header">
  <div class="col-sm-12" style="text-align:center;">
  <div style="float:right;padding-top:15px;">
    <span class="label label-success" onclick="getFirstLocation();" id="calibrating">
      <i class="fa fa-location-arrow"></i>&nbsp; Calibrate My Location
    </span>
  </div>
  <div style="float:left;padding:11px 20px 0px 5px;font-size:15px;">
  SCHOOLBIRD
  </div>
  </div>
</div>
<div style="height:40px"></div>
<div class="row">
  <div class="col-sm-2 sideMenu" id="sideMenuBig" style="">
  <div style="position:absolute;bottom:100px;left:10px;width:90%;text-align:center;display:none">
    <img src="images/logo.png" alt="" style="width:70%;margin-bottom:10px;"/>
    <br/>
        <span style="font-size:11px;">The sprit of connectivity</span>
  </div>

  <?php
  
  
include("menu.php");

  ?>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  </div>
  <div class="col-sm-10" style="padding:0px !important" id="rightContainer">
     <div id="tableDiv" style="overflow-y:auto;padding:10px">
    </div>
    <div id="formDiv" style="display:none;overflow-y:auto;padding:10px"></div>
    </div>

  </div>
</div>

<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

</body>
<script type="text/javascript">
  var inHeight = window.innerHeight;
  $('#sideMenuBig').animate({height:inHeight},50);
  $('#formDiv').animate({height:inHeight},50);
  $('#tableDiv').animate({height:inHeight},50);
//  document.getElementById('sideMenuBig').style.height = window.innerHeight;

//showProcessing();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places"></script>
</html>
