function getModal(url,showresponse,hideresponse,loading)
{
var loadingVars;
	var checkLoadingType = loading.indexOf(":");
	if(checkLoadingType == -1)
	{
		showProcessing();
	}
	else
	{
		loadingVars = loading.split(":");
		$('#'+loadingVars[0]).html(loadingVars[1]);
	}

        $.post(url,
        {
          name: "Donald Duck",
          city: "Duckburg"
        },
        function(data,status){
        if(showresponse.indexOf('ModalBig') == -1)
        {
        $("#myModal").modal();
        }
        else
        {
        $("#myModalBig").modal();
        }
          //  alert("Data: " + data + "\nStatus: " + status);
          document.getElementById(showresponse).innerHTML = data;
          $('#'+showresponse).show();
          if(hideresponse != '')
          {
      	    $('#'+hideresponse).hide();
          }
          
		    if(checkLoadingType == -1)
			{
				hideProcessing();
			}
			else
			{
				loadingVars = loading.split(":");
				$('#'+loadingVars[0]).html(loadingVars[2]);
			}

        });
}


/*
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
*/