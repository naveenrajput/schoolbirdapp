function massEdit()
{
	var list = $('#massEdit').val();
	var url = "massedit/masseditform.php?list="+list;
	var orgHTML = $('#bulkGoButton').html();
	disableButton('bulkGoButton','Opening');
	genAjax(url,'',function(response){
		enableButton('bulkGoButton',orgHTML);
		$('#modalData').html(response);
		$('#massEditModal').modal();
	});
}

function getMassEditInput(value)
{
	temp = value.split(":");
	if(temp.length == 4)
	{
	
		var table= temp[2];
		var field= temp[1];
		if(temp[3] == 'text')
		{
		var html = "<br/><br/><input value='' id='massEditValue' placeholder='Enter Value For "+temp[0]+"' class='input' /><br/><br/>";			
		}
		if(temp[3] == 'date')
		{
		var html = "<br/><br/><input value='' type='date' id='massEditValue' placeholder='Enter Value For "+temp[0]+"' class='input' /><br/><br/>";			
		}

		else if(temp[3] == 'textarea')
		{
		var html = "<br/><br/><textarea id='massEditValue' class='input' style='height:100px;width:300px;'>Enter Value For "+temp[0]+"</textarea><br/><br/>";			
		}
		else if(temp[3].indexOf('select') != -1)
		{

		var html = "<br/><br/><select id='massEditValue' class='input'><option>Select Value</option>";						
			temp1 = temp[3].split("select{");
			temp2= temp1[1].split("}");
			temp3 = temp2[0].split("$$$");
			for(h=0;h<temp3.length;h++)
			{
				html+= "<option value='"+temp3[h]+"'>"+temp3[h]+"</option>";
			}
			html+= "</select><br/><br/>";
		}
		html += "<button class='btn btn-primary btn-sm' id='massupdatebutton' onclick=\"callMassEdit('"+table+"','"+field+"',document.getElementById('massEditValue').value)\">Update</button>";
		$('#meicont').html(html);
	}
	else if(temp.length == 5)
	{
		var table= temp[2];
		var field= temp[1];
		var constraintTable = temp[3];
		var url = "massedit/getconstrainttable.php?table="+constraintTable;
		genAjax(url,'',function(response){
			html = "<br/><br/>"+response+"<br/><br/>";
			html += "<button class='btn btn-primary btn-sm' id='massupdatebutton' onclick=\"callMassEdit('"+table+"','"+field+"',document.getElementById('massEditValue').value)\">Update</button>";
		$('#meicont').html(html);
		});
	}

}

function callMassEdit(table,field,value)
{
	if(value != '')
	{
		var checkList = "0";
		var itemSearch = $('.checkInput');
		$('#tableDiv').find(itemSearch).each(function(){
			if(this.checked == true)
			{
				var thisVal = this.value;
				checkList = checkList+","+thisVal;
			}
				
	    });	

	    if(checkList == 0)
	    {
			showBottomNotification('stop','You have to select atleast one row to edit, else there\'s no point for me to have a mass edit option');
	    }
	    else
	    {
		    var params = {
		    	idList:checkList,
		    	table:table,
		    	field:field,
		    	value:value
		    };
		    
		//    var params = "idList="+checkList+"&table="+table;
		var orgHTML = $('#massupdatebutton').html();
		disableButton('massupdatebutton','Updating..');
		    var url = "massedit/doMassEdit.php";
		    genAjax(url,params,function(response){
		   	enableButton('massupdatebutton',orgHTML);
		    showBottomNotification('success','Successfully updated <strong>"'+field+'"</strong> value for selected records');		
		    });
	    }   
	}
	else
	{
		showBottomNotification('stop','You have to enter a value for '+field+' else there\'s no point for me to have a mass edit option');
	}
}