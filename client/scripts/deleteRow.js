function crossCheckDelete(table)
{
	var comfirmMessage = 'Do you really want to delete selected rows?<br/><br/><button type="button" class="btn btn-primary btn-sm"  onclick="deleteRow(\''+table+'\')" id="confirmDelete"><span class="glyphicon glyphicon-alert"></span>&nbsp;&nbsp;Yes!</button>&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-sm"  onclick="$(\'#errorModal\').modal(\'hide\');showBottomNotification(\'success\',\'Good one. There is no point deleting what is actually in system. :) :)\')"><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;No</button>';
showError(comfirmMessage,'',0);
}
function deleteRow(table)
{
	var checkList = "0";
	var itemSearch = $('.checkInput');
	$('#tableDiv').find(itemSearch).each(function(){
		if(this.checked == true)
		{
			var thisVal = this.value;
			if(document.getElementById('tableRow'+thisVal))
			{
				$('#tableRow'+thisVal).hide();
				//document.getElementById('tableRow'+thisVal).style.display = 'none';				
			}
			checkList = checkList+","+thisVal;
		}
			
    });	
//    getModule("deleteRow.php?table="+table+"&ids="+checkList,'','','');
disableButton('confirmDelete','Deleting..');
var url = "deleteRow.php?table="+table+"&ids="+checkList;
genAjax(url,'',function(response){

	$('#errorModal').modal('hide');
var html = "Successfully deleted all the selected rows.&nbsp;&nbsp;<span style='text-decoration:underline;cursor:pointer' onclick=\"retrieveRow('"+table+"','"+checkList+"')\">Undo</span>";
showBottomNotification('successdel',html);
});

}

function retrieveRow(table,checkList)
{
	
$('#loading').show();

var url = "retrieveRow.php?table="+table+"&ids="+checkList;
	genAjax(url,'',function(response){
	$('#loading').hide();
	var rowList = checkList.split(",")
	for(j=0;j<rowList.length;j++)
	{
		var thisVal = rowList[j];
			if(document.getElementById('tableRow'+thisVal))
			{
				$('#tableRow'+thisVal).show();
				//document.getElementById('tableRow'+thisVal).style.display = 'table-row';				
			}

	}
	var html = "Successfully retreived all rows deleted in previous action.";
	showBottomNotification('success',html);
	});

}



