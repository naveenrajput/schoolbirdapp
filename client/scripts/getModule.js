var globalSendParam;
function getModule(url,showresponse,hideresponse,loading)
{
if(showresponse == 'formDiv')
{
  document.getElementById('tableModalBig').innerHTML = '';
  document.getElementById('formModalBig').innerHTML = '';
}
  if(document.getElementById(showresponse))
  {
	var loadingVars = [];
	var checkLoadingType = loading.indexOf(":");
  
  /*
  historyArray[historyVal] = url+"&shw="+showresponse+"&hr="+hideresponse+"&loading="+loading;
  window.location.hash = historyVal;
  historyVal++;
  */
  if(outerChange != 1)
  {
    outerChange = 0;
    innerChange = 1;
    window.location.hash = "&rtl="+url+"&rtl="+showresponse+"&rtl="+hideresponse+"&rtl="+loading;
  }
  else
  {
   outerChange = 0; 
  }
  
  if(checkLoadingType == -1)
	{
	showProcessing();
  //	$('#'+loading).show();
	}
	else
	{
		loadingVars = loading.split(":");
		$('#'+loadingVars[0]).html = loadingVars[1];
	}

        $.post(url,
        {
        globalSendParam
        },
        function(data,status){
          globalSendParam = '';
          document.getElementById(showresponse).innerHTML = data;
          /*
                        $(document).ready(function(){
                        $('[data-toggle="tooltip"]').tooltip(); 
                           });
                           */
          if(url.indexOf('buslist.php') != -1)
          {
            var thisinHeight = window.innerHeight - 300;
              $('#busLister').animate({height:thisinHeight},50);
              document.getElementById('sideMenuTable').className = 'sideMenuTableSmall';
              document.getElementById('sideMenuBig').className = 'col-sm-1 sideMenu';
              document.getElementById('rightContainer').className = 'col-sm-11';

          }
          else
          {
              document.getElementById('sideMenuTable').className = 'sideMenuTable';
              document.getElementById('sideMenuBig').className = 'col-sm-2 sideMenu';
              document.getElementById('rightContainer').className = 'col-sm-10';            
          }
          if(showresponse == 'tableDiv')
          {
              var itemSearch = $('.searchInput');
              searchList = $('#tableDiv').find(itemSearch).each(function(){
                $(this).keyup(function() {
              tableSearch(event,url)
              });
            });  
          }
          else if(showresponse == 'rcpresponse')
          {
            $(function(){
            $("#rcpTable").tablesorter({
              theme : 'blue',
              sortList : [[0,0]],             
                // header layout template; {icon} needed for some themes
                headerTemplate : '{content}{icon}',
             
              // initialize column styling of the table
                widgets : ["columns"],
              widgetOptions : {
                  // change the default column class names
                  // primary is the first column sorted, secondary is the second, etc
                  columns : [ "primary", "secondary", "tertiary" ]
              }
            });
            });
            
          }

          $('#'+showresponse).show() 
          if(hideresponse != '')
          {
      	    $('#'+hideresponse).hide();
          }
          
		    if(checkLoadingType == -1)
			{
				hideProcessing();
        //$('#'+loading).hide();
			}
			else
			{
				loadingVars = loading.split(":");
				$('#'+loadingVars[0]).html = loadingVars[2];
			}

        });
        }
        else
        {
          showError('The system is modified since last update. Please <a href="default.php" style="color:#b82121;text-decoration:underline"><span class="glyphicon glyphicon-reload"></span>Reload</a> to continue')
        }
    }


    function genAjax(url,params,callback)
    {

$.ajax({
  type: "POST",
  url: url,
  data: params,
  success: function(data){
  hideToast();
       callback(data);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    if(document.getElementById('toast').innerHTML.indexOf('Retrying') != -1)
    {
      setTimeout(function(){
   toast('Unable to connect to SchoolBird Server. Please check your network connection. Will retry in a few moments','toast bg-danger','0');
      },2000);
    }
    else
    {
         toast('Unable to connect to SchoolBird Server. Please check your network connection. Will retry in a few moments','toast bg-danger','0');
    }

   setTimeout(function(){
    toast('Retrying..','toast bg-info','0');
genAjax(url,params,callback);
   },5000);
  }
});

    }



var loadInterval;

    function showProcessing()
    {
      
      $('#stopButton').hide();
      $('#loading').show();
      document.getElementById('loadbar').style.width = "0%";
$('#loadbar').animate({width:'85%'},10000,function(){
$('#stopButton').show();
}); 
      /*
      clearInterval(loadInterval);
      d
      loadInterval = setInterval(function(){
        var x = document.getElementById('loadbar').style.width;
        x = x.replace("%","");
        x = x+4;
        document.getElementById('loadbar').style.width = x+"%";
      },2000);
      */
    }


    function hideProcessing()
    {
    $('#loadbar').stop();
    
      $('#loadbar').animate({width:'100%'},300);
      setTimeout(function(){
      $('#loading').hide();     
    },1000);
    }
