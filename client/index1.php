<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::PARENT EYE CLIENT LOGIN::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/w3.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/getModule.js"></script>
<script type="text/javascript" src="scripts/misc.js"></script>
<script type="text/javascript" src="scripts/getModal.js"></script>  

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>
<script type="text/javascript">
function loginNow()
{
  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  if(username == '' || password == '')
  {
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      
      return false;
  }
  var url = "login.php";
  var params = {
    username:username,
    password:password
  }
  document.getElementById('logButton').innerHTML = 'AUTHENTICATING..';
  genAjax(url,params,function(response){
    if(response == '0')
    {
      document.getElementById('logButton').innerHTML = 'RETRY LOGIN <i class="fa fa-arrow-right"></i>';
      toast("Invalid login details, please try again.",'toast bg-danger','3000');      

    }
    else
    {
      document.getElementById('logButton').innerHTML = 'REDIRECTING..';
     window.location = "default.php";
    }
  });

}
</script>
</head>
<body>
<div class="row footer">
  <div class="col-sm-12" style="font-size:10px;color:#888">
  <center>
    <hr>
    Copyright 2016, All Rights Reserved.<br/>
    <a href="#">
    AcesSkyNet Pvt Ltd.
    </a>
    <br/>
    <br/>
  </center>
  </div>
</div>
<div class="row">
  <div class="col-sm-12 logHeadBack" style="text-align:center;padding-top:40px;padding-bottom:80px;">
    <div style="width:350px;text-align:left;display:inline-block;color:#fff;">
    <img src="images/logo.png" alt="" style="width:60%;">
<br/><br/>
    <h2>Welcome to ParentEye client portal.</h2>
    <hr style="width:200px;">
    <span style="font-size:11px;">A Product By Aces Skynet Pvt Ltd.</span>
    <br/>
        <span style="font-size:11px;">The sprit of connectivity</span>
    <br/>
    <br/>
    <br/>

    <h4>Login to your account.</h4>
    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="text" style="width:100%" id="username" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-envelope"></i>&nbsp;&nbsp;Email Id</label>
    </div>

    <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1b" type="password" style="width:100%" id="password" required="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
      <i class="fa fa-lock"></i>&nbsp;&nbsp;Passcode</label>
    </div>
 <button class="btn btn-sm btn-danger" style="width:50%" id="logButton" onclick="loginNow();">TAKE ME IN <i class="fa fa-arrow-right"></i></button>


    </div>
  </div>
</div>


<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

<script type="text/javascript">
  hideToast();
</script>
</body>
</html>
