<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
           <script src="scripts/chartist-plugin-tooltip.js"></script>
    <style type="text/css">
    	
    	.ct-series-a .ct-line {
  /* Set the colour of this series line */
  stroke: #076bff;
  /* Control the thikness of your lines */
  stroke-width: 3px;
  /* Create a dashed line with a pattern */
  
}

.ct-series-a .ct-point {
  /* Colour of your points */
  stroke: #0d59c4;
  /* Size of your points */
  stroke-width: 10px;
  /* Make your points appear as squares */
  stroke-linecap: circle;
}

.chartist-tooltip {
  position: absolute;
  display: inline-block;
  opacity: 0;
  min-width: 5em;
  padding: .5em;
  background: #F4C63D;
  color: #453D3F;
  font-family: Oxygen,Helvetica,Arial,sans-serif;
  font-weight: 700;
  text-align: center;
  pointer-events: none;
  z-index: 1;
  -webkit-transition: opacity .2s linear;
  -moz-transition: opacity .2s linear;
  -o-transition: opacity .2s linear;
  transition: opacity .2s linear; }
  .chartist-tooltip:before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    width: 0;
    height: 0;
    margin-left: -15px;
    border: 15px solid transparent;
    border-top-color: #F4C63D; }
  .chartist-tooltip.tooltip-show {
    opacity: 1; }

.ct-area, .ct-line {
  pointer-events: none; }

/*# sourceMappingURL=chartist-plugin-tooltip.css.map */
    </style>
  </head>
  <body>
  	<div class="ct-chart ct-perfect-fourth" style="height:200px;width:100%"></div>
  	<script type="text/javascript">
var chart = new Chartist.Line('.ct-chart', {
  labels: [1, 2, 3],
  series: [
    [
      {meta: 'description', value: 3 },
      {meta: 'description', value: 5},
      {meta: 'description', value: 3}
    ],
    [
      {meta: 'other description', value: 2},
      {meta: 'other description', value: 4},
      {meta: 'other description', value: 2}
    ]
  ]
}, {
  low: 0,
  high: 8,
  fullWidth: true,
  plugins: [
    Chartist.plugins.tooltip()
  ]
});

  	</script>


<button onclick="updateChart();">Update Chart</button>


  </body>
</html>