<?php
include("../include/config.php");
$date = $_POST['date'];
$class = $_POST['class'];
$getData = mysqli_query($con,"SELECT student.name,attendance.status FROM student,attendance WHERE student.id = attendance.student AND student.class = '$class' AND attendance.date = '$date'") or die(mysqli_error($con));
?>

    <table class="table table-striped table-hovered fetch">
        <tr>
            <th>#</th><th>Name</th><th>Status</th>
        </tr>
<?php
$j=1;
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>
		<td><?php echo $j;?></td>
		<td><?php echo $row[0];?></td>
		<td><?php 
		if($row[1] == 'P')
		{
			?>
			<span class="label label-success">PRESENT</span>
			<?php
		}
		else
		{
			?>
			<span class="label label-danger">ABSENT</span>
			<?php

		}

		?></td>
	</tr>
	<?php
	$j++;
}
?>
</table>
