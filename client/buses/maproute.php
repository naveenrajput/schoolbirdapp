<?php
include("../include/config.php");
$id = $_GET['id'];
$table = "buses";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>
<div class="row">
	
<div class="col-sm-12">

<div class="moduleHead">
		

	<div class="moduleHeading">
Map Routes

	</div>
</div>
<center>
<br/>

<div id="routeSpecialSaved" class="btn btn-sm btn-success" style="display:none"></div>
</center>
<div class="row">
<div class="col-sm-12">
	<div style="float: right">
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('buses/saveroute.php?id=<?php echo $id;?>','','','rt',5,'','routeSpecialSaved','tableModalBig','formModalBig');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
&nbsp;&nbsp;
		<button class="btn btn-danger btn-sm" onclick="$('#myModalBig').modal('hide')" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			CLOSE</button>

			
			</div>
</div>

<div class="col-sm-12">


	 <div class="w3-group margin10" style="width:100%">      
	 <select class="w3-input input1bdark" style="width:100%" name=""  required="" id="rt0" onchange="if(this.value == '') { $('#dateSelector').show(); } else { $('#dateSelector').hide(); }">
	 	<option value="MON">MON</option>
	 	<option value="TUE">TUE</option>
	 	<option value="WED">WED</option>
	 	<option value="THU">THU</option>
	 	<option value="FRI">FRI</option>
	 	<option value="SAT">SAT</option>
	 	<option value="SUN">SUN</option>
	 	<option value="">SELECT DATE</option>
	 </select>

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Select Day</label>
    </div>
</div>


<div class="col-sm-12" style="display:none" id="dateSelector">
	 <div class="w3-group margin10" style="width:100%">      
  <input class="w3-input input1bdark" type="date" style="width:100%" name="req"  required="" id="rt1" value="2016-01-01">

      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Enter Date</label>
    </div>
</div>

<div class="col-sm-12">
		<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="rt2">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="rt3" value="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="rt4" value="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
</div>


</div>



</div>

</div>