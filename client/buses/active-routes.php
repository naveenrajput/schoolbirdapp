<?php
include("../include/config.php");
if($_GET['bus'] == 'ALL')
{
$getData = mysqli_query($con,"SELECT buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day FROM buses,routes,busroutemap WHERE buses.id = busroutemap.busid AND routes.id = busroutemap.routeid ORDER BY buses.bus ASC") or die(mysqli_error($con));
}
else
{
	$bus = $_GET['bus'];
$getData = mysqli_query($con,"SELECT buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day FROM buses,routes,busroutemap WHERE buses.id = busroutemap.busid AND routes.id = busroutemap.routeid AND buses.id = '$bus' ORDER BY buses.bus ASC") or die(mysqli_error($con));	
}

?>
<div class="moduleHead">
<div class="moduleHeading">
Active Bus Routes
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">

<div style="padding:10px;">
Filter Bus&nbsp;&nbsp;<select class="input1bdark" id="filtBus"  style="width:200px;">
<option <?php if($bus == 'ALL') echo "selected='selected'" ;?> value="ALL">ALL BUSES</option>
<?php
$getBuses = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus`") OR die(mysqli_error($con));
while($rowBuses = mysqli_fetch_array($getBuses))
{
?>
<option <?php if($bus == $rowBuses['id']) echo "selected='selected'" ;?> value="<?php echo $rowBuses['id'];?>"><?php echo $rowBuses['bus'];?></option>
<?php
}
?>

</select>
&nbsp;&nbsp;

<button class="btn btn-sm btn-primary" onclick="getModule('buses/active-routes.php?bus='+document.getElementById('filtBus').value,'tableDiv','formDiv','loading')">GO&nbsp;&nbsp;
<i class="fa fa-arrow-right"></i>
</button>
</div>
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">

<tr>
	<th>Bus</th>
	<th>Route</th>
	<th>Day</th>
	<th>From</th>
	<th>To</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>
<td><?php echo $row[0];?></td>
<td><?php echo $row[1];?></td>
<td><?php echo $row[4];?></td>
<td><?php echo $row[2];?></td>
<td><?php echo $row[3];?></td>
</tr>
	<?php
}
?>
</table>
</div>