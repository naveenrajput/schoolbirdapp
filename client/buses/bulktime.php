<?php
include("../include/config.php");
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}

$getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));
//print_r($loggeduserid);die;
?>
<div class="moduleHead">
<div class="moduleHeading">
Bulk Update Timing
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table style="width:100%;">
	<tr>
		<td style="width:25%" valign="top">
<div class="moduleHeading" style="padding:10px !important;text-decoration:none !important">
Select Buses
</div>

		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
			<?php
			$tbus = 0;
$getBuses = mysqli_query($con,"SELECT * FROM `buses`") or die(mysqli_error($con));
while($rowBuses = mysqli_fetch_array($getBuses))
{
	?>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" id="tbus<?php echo $tbus;?>" value="<?php echo $rowBuses['id'];?>">
				</td>
				<td>
					<?php echo $rowBuses['bus'];?>
				</td>
			</tr>

	<?php
	$tbus++;
}
			?>
		</table>
<input type="text" id="tbus" name="" value="<?php echo $tbus;?>" style="display:none !important;">
		</td>

<td style="width:25%" valign="top">

<div class="moduleHeading" style="padding:10px !important;text-decoration:none !important">
Select Days
</div>

		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="MON" id="day0">
				</td>
				<td>
					MONDAY
				</td>

			</tr>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="TUE" id="day1">
				</td>
				<td>
					TUESDAY
				</td>

			</tr>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="WED" id="day2">
				</td>
				<td>
					WEDNESDAY
				</td>

			</tr>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="THU" id="day3">
				</td>
				<td>
					THURSDAY
				</td>

			</tr>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="FRI" id="day4">
				</td>
				<td>
					FRIDAY
				</td>

			</tr>
			<tr>
				<td stylw="width:20px">
					<input type="checkbox" value="SAT" id="day5">
				</td>
				<td>
					SATURDAY
				</td>

			</tr>

		</table>

		</td>
		<td style="width:50%" valign="top">
			<div class="shadow">
<div class="row">


<div class="col-sm-12">
		<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="routeType">
				<option value="M">MORNING PICKUP</option>
				<option value="A">AFTERNOON (~Junior Shift)</option>
				<option value="E">AFTERNOON (~Senior Shift)</option>
				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Route Type</label>
    </div>
</div>

<div class="col-sm-12">
		<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="selRoute">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
</div>

<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="routeFromTime" value="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
</div>

<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="routeToTime" value="">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
</div>




<div class="col-sm-12">
<button class="btn btn-primary" onclick="saveBulkRouteBuses()">SAVE DATA</button>
<br/>
<br/>

</div>



</div>
</div>

		</td>
	</tr>

</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>