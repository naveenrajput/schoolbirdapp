<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));

?>
<div class="moduleHead">
<div style="float:right">

&nbsp;&nbsp;&nbsp;&nbsp;
<?php if($loggeduserid!=8){?>
<button class="btn btn-primary" onclick="getModule('buses/new.php','formDiv','tableDiv','loading')">ADD NEW</button>&nbsp;&nbsp;
<div class="dropdown" style="display:inline-block;margin-right:30px;">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">BUS ACTIONS
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li   onclick="getModule('buses/schooltiming.php','tableDiv','formDiv','loading')">SCHOOL TIMING UPDATE</li>
    <li  onclick="getModule('buses/bulktime.php','tableDiv','formDiv','loading')">BULK TIMING UPDATE</li>
    <li onclick="getModule('buses/replace.php','formDiv','tableDiv','loading')">BUS REPLACEMENT</li>
    <li  onclick="getModule('buses/class-leave.php','formDiv','tableDiv','loading')">CLASS LEAVE</li>
    <li  onclick="getModule('buses/active-routes.php?bus=ALL','tableDiv','formDiv','loading')">ACTIVE ROUTES</li>
  </ul>
</div>
<?php }?>


</div>
<div class="moduleHeading">
Buses
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Bus No.</th>
<th>Driver</th>
<th>Mobile</th>
<th>Locate</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary" onclick="getModule('buses/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['bus'];?></td>
<td><?php echo $row['drivername'];?></td>
<td><?php echo $row['drivernumber'];?></td>
<td onclick="window.open('tracking/trackMap.php?buslist=<?php echo $row['id'];?>,','_blank')">
	<span class="label label-primary"><i class="fa fa-map-marker"></i> VIEW ON MAP</span>
</td>

</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>