<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
<button class="btn btn-sm btn-info"  onclick="getModule('buses/editschooltimings.php','formDiv','tableDiv','loading')">EDIT TIMINGS</button>


</div>
<div class="moduleHeading">
School Timing Update
</div>
</div>
<div class="row">
	<div class="col-ms-6">
	<div class="tabelContainer divShadow" style="height:auto">
		<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
		<tr>
			<th colspan="3">Current Timings</th>
		</tr>
		<tr>
			<th>From Time</th>
			<th>To Time</th>
			<th>Type</th>
		</tr>
		<?php
$getData = mysqli_query($con,"SELECT * FROM `busroutemap` GROUP BY `from`") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	?>
<tr>
	<td><?php echo $row['from'];?></td>
	<td><?php echo $row['to'];?></td>
	<td><?php
	if($row['type'] == 'A')
	{
		echo "Afternoon Junior Drop";
	}
	else if($row['type'] == 'M')
	{
		echo 'Morning Pickup';
	}
	else if($row['type'] == 'E')
	{
		echo 'Afternoon Senior Drop';
	}

	?></td>
</tr>
	<?php
}
		?>
		
			
		</tr>
		</table>

</div>

	</div>
</div>


<br/><br/><br/><br/><br/><br/><br/><br/><br/>