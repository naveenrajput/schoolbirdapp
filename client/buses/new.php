<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','bus',5,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			</div>

	<div class="moduleHeading">
		Add A New Bus

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus0">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Bus No.</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus1">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus2">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Mobile
</div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus3">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Name</label>
    </div>
</div>
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus4">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Mobile</label>
    </div>
</div>


</div>
</div>



<br />
<br />
<br />
<br />
<br />
