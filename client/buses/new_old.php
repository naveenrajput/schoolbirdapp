<?php
include("../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','bus',35,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			</div>

	<div class="moduleHeading">
		Add A New Bus

	</div>
</div>
<div class="shadow">
<div class="row">
<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus0">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Bus No.</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus1">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus2">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Mobile
</div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus3">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Name</label>
    </div>
</div>
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus4">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Mobile</label>
    </div>
</div>

<div class="col-sm-12">
	<table class="table table-striped table-hovered" style="border:1px #eee solid">
	<tr>
		<th style="width:10%">Day/Date</th>
		<th style="width:20%">Morning Route</th>
		<th colspan="2" style="width:20%">Morning Time</th>
		<th style="width:20%">Afternoon Route</th>
		<th colspan="2" style="width:20%">Afternoon Time</th>
	</tr>
		

		<tr>
			<td>
				
				<div class="circleBig" style="background:#31BEDB;">
					MON
				</div>

			</td>
			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus5">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus6">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus7">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus5">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>

			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus8">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus9">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>

		</tr>

		<tr>
			<td>
				
				<div class="circleBig" style="background:#47C9AD">
					TUE
				</div>

			</td>

			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus10">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus11">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus12">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus13">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus14">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
		</tr>	

		<tr>
			<td>
				
				<div class="circleBig" style="background:#FECE33">
					WED
				</div>

			</td>

			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus15">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus16">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus17">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus18">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus19">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
		</tr>

		<tr>
			<td>
				
				<div class="circleBig" style="background:#239F86">
					THU
				</div>

			</td>

			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus20">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus21">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus22">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus23">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus24">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
		</tr>

		<tr>
			<td>
				
				<div class="circleBig" style="background:#FDA529">
					FRI
				</div>

			</td>
			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus25">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus26">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus27">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus28">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus29">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
		</tr>

		<tr>
			<td>
				
	<div class="circleBig" style="background:#3081B7">
					SAT
				</div>
	

			</td>
			<td>

			<div class="w3-group margin10" style="width:100%">      
     
				<select class="w3-input input1bdark" required="" style="width:100%;height:47px" id="bus30">
				<option value="">No Route</option>
					<?php
						foreach($routes as $key => $val)
						{
							?>
<option value="<?php echo $routeIds[$key];?>"><?php echo $val;?></option>
							<?php
						}
					?>

				</select>
			 <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Select Route</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus31">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus32">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>


			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus33">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    From Time</label>
    </div>
			</td>
			<td>
				 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="time" style="width:100%"   required="" id="bus34">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    To Time</label>
    </div>
			</td>
		</tr>
		

	</table>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/><br/>
</div>


</div>
</div>



<br />
<br />
<br />
<br />
<br />
