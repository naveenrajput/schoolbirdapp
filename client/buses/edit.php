<?php
include("../include/config.php");
$updateurl = str_ireplace("edit.php", "update.php", $urltocall);
$callbackurl = str_ireplace("edit.php", "index.php", $urltocall);
$id = $_GET['id'];
$table = "buses";
$getData = mysqli_query($con, "SELECT * FROM `$table` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$routes = Array();
$routeIds = Array();
$getRoutes = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error());
while($rowRoutes = mysqli_fetch_array($getRoutes))
{
	$routes[] .= $rowRoutes['name'];
	$routeIds[] .= $rowRoutes['id'];
}
?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-danger btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>&nbsp;&nbsp;
			<?php if($loggeduserid == 8 || $loggeduserid == 9){}else{?>
			
		<button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $row['id'];?>','buses');" type="button">
			<i class="fa fa-remove"></i>&nbsp;&nbsp;
			DELETE ENTRY</button>&nbsp;&nbsp;

			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-info btn-sm" onclick="getModal('buses/routes.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')" type="button">
			<i class="fa fa-calendar"></i>&nbsp;&nbsp;ROUTES</button>&nbsp;&nbsp;
			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-info btn-sm" onclick="getModal('buses/maproute.php?id=<?php echo $id;?>','tableModalBig','formModalBig','')" type="button">
			<i class="fa fa-calendar"></i>&nbsp;&nbsp;MAP ROUTE</button>&nbsp;&nbsp;

			<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $updateurl;?>','','','bus',5,'','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE RECORD</button>
            <?php } ?>
			
			</div>

	<div class="moduleHeading">
	Bus Details

	</div>
</div>


<div class="shadow">
<div class="row">
<div class="col-sm-12">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus0" value="<?php echo $row['bus'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
     Bus No.</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus1" value="<?php echo $row['drivername'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Name</label>
    </div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%" name="req"  required="" id="bus2" value="<?php echo $row['drivernumber'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Driver Mobile
</div>
</div>

<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus3" value="<?php echo $row['conductorname'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Name</label>
    </div>
</div>
<div class="col-sm-6">
	 <div class="w3-group margin10" style="width:100%">      
      <input class="w3-input input1bdark" type="text" style="width:100%"   required="" id="bus4" value="<?php echo $row['conductornumber'];?>">
      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
    Conductor Mobile</label>
    </div>
</div>




</div>
</div>



<br />
<br />
<br />
<br />
<br />
