<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
    <?php if($loggeduserid!=8){?>
<div style="float:right">
	<button class="btn btn-sm btn-danger"  onclick="getModule('routes/new.php','formDiv','tableDiv','loading')">+1 ADD NEW ROUTE</button>&nbsp;&nbsp;

		<button class="btn btn-sm btn-danger" onclick="$('#cloneBox').slideDown();">CLONE ROUTE</button>
		&nbsp;&nbsp;
	<button class="btn btn-sm btn-danger"  onclick="getModule('routes/active-buses.php?route=ALL','formDiv','tableDiv','loading')">ACTIVE BUSES</button>&nbsp;&nbsp;
		
</div>
<?php }?>



<div class="moduleHeading">
Routes
</div>

<div id="cloneBox" style="padding:10px;background:#fff;border:1px #ddd solid;display:none" class="shadow">
<div style="float:right;padding:10px;cursor:pointer" onclick="$('#cloneBox').slideUp();">
	<i class="fa fa-close"></i>
</div>
Please select route to clone.
<br/>
<br/>
		<select class="w3-input input1bdark" style="width:100%;"  name="req" required="" id="cloner">
			<?php
				$getData1 = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name` ASC") or die(mysqli_error($con));
				while($rowData = mysqli_fetch_array($getData1))
				{
					?>
					<option value="<?php echo $rowData['id'];?>"><?php echo $rowData['name'];?></option>
					<?php
				}
			?>
		</select>  
		<br/>
		<button class="btn btn-primary" onclick="getModule('routes/clone.php?pre='+document.getElementById('cloner').value,'formDiv','tableDiv','loading')">GO</button>

		</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>#Stops</th>
<th>View on Map</th>
<th>Createdate</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>" onclick="getModule('routes/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')">
<td><?php echo $j+1;?></td>
<td class="text-primary"><?php echo $row['name'];?></td>
<td><?php echo $row['stopcount'];?></td>
<td>
	<span class="label label-primary"><i class="fa fa-map-marker"></i> VIEW ON MAP</span>
</td>
<td><?php echo date("d-m-y",strtotime($row['createdate']));?></td>
</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>