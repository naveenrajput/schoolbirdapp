<?php
include("../include/config.php");
if($_GET['route'] == 'ALL')
{
$getData = mysqli_query($con,"SELECT buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day FROM buses,routes,busroutemap WHERE buses.id = busroutemap.busid AND routes.id = busroutemap.routeid ORDER BY routes.name ASC") or die(mysqli_error($con));
}
else
{
	$route = $_GET['route'];
$getData = mysqli_query($con,"SELECT buses.bus,routes.name,busroutemap.from, busroutemap.to, busroutemap.day FROM buses,routes,busroutemap WHERE buses.id = busroutemap.busid AND routes.id = busroutemap.routeid AND routes.id = '$route' ORDER BY routes.name ASC") or die(mysqli_error($con));	
}

?>
<div class="moduleHead">
<div class="moduleHeading">
Active Route Buses
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">

<div style="padding:10px;">
Filter Routes&nbsp;&nbsp;<select class="input1bdark" id="filtBus"  style="width:200px;">
<option <?php if($bus == 'ALL') echo "selected='selected'" ;?> value="ALL">ALL ROUTES</option>
<?php
$getBuses = mysqli_query($con,"SELECT * FROM `routes` ORDER BY `name`") OR die(mysqli_error($con));
while($rowBuses = mysqli_fetch_array($getBuses))
{
?>
<option <?php if($route == $rowBuses['id']) echo "selected='selected'" ;?> value="<?php echo $rowBuses['id'];?>"><?php echo $rowBuses['name'];?></option>
<?php
}
?>

</select>
&nbsp;&nbsp;

<button class="btn btn-sm btn-primary" onclick="getModule('routes/active-buses.php?route='+document.getElementById('filtBus').value,'formDiv','tableDiv','loading')">GO&nbsp;&nbsp;
<i class="fa fa-arrow-right"></i>
</button>
</div>
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">

<tr>
	
	<th>Route</th>
	<th>Bus</th>
	<th>Day</th>
	<th>From</th>
	<th>To</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>

<td><?php echo $row[1];?></td>
<td><?php echo $row[0];?></td>
<td><?php echo $row[4];?></td>
<td><?php echo $row[2];?></td>
<td><?php echo $row[3];?></td>
</tr>
	<?php
}
?>
</table>
</div>