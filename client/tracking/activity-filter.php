<div style="padding:20px;">

<div class="row">

<div class="col-sm-6" style="padding:0px ;">
<div class="w3-group margin10" style="width:100%">  
    <select class="w3-input input1bdark" style="width:100%;height:47px" id="actBus" name="bus" required=""  name="req">
    <option value="0">Select Bus</option>
    <?php
    $getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));
    while($row = mysqli_fetch_array($getData))
    {
      ?>
      <option value="<?php echo $row['id'];?>"><?php echo $row['bus'];?></option>
      <?php     
    }
    ?>
    </select>  
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Bus</label>
    </div>
</div>
<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="w3-input input1bdark" type="date" style="width:100%" id="actDate" name="date" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         For Date</label>
    </div>
</div>

<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="w3-input input1bdark" type="time" style="width:100%" id="actFrom" name="fromtime" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         From Time</label>
    </div>
</div>


<div class="col-sm-6">
  <div class="w3-group margin10" style="width:100%">      
          <input class="w3-input input1bdark" type="time" style="width:100%" id="actTo" name="totime" required="">
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         To Time</label>
    </div>
</div>

<div class="col-sm-6">
  <button class="btn btn-primary" onclick="drawActivityChart('in')">VIEW HERE&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
&nbsp;&nbsp;&nbsp;
    <button class="btn btn-info" onclick="drawActivityChart('out')">VIEW IN NEW WINDOW&nbsp;&nbsp;<i class="fa fa-external-link"></i></button>


</div>
<div class="col-sm-12">
<hr>
  <iframe style="width:100%;height:500px" id="activityResult" frameborder="0" scrolling="no"></iframe>
</div>
  </div>


</div>