<?php
include("../include/config.php");
$buslist = $_GET['buslist'];
$buslist = substr($buslist,0,-1);
$busArray = Array();
$keyArray = Array();
$idArray = Array();
$driverArray = Array();
$driverMobileArray = Array();


$getData  = mysqli_query($con,"SELECT * FROM `buses` WHERE `id` IN ($buslist)") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
if($row['key'] != '')
{
$busString .= $row['bus'].",";
$keyString .= $row['key'].",";
$busArray[] .= $row['bus'];
$keyArray[] .= $row['key'];
$idArray[] .= $row['id'];
$driverArray[] .= $row['drivername'];
$driverMobileArray[] .= $row['drivernumber'];
}
}
$busString  = substr($busString,0,-1);
$keyString  = substr($keyString,0,-2);


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.::TRACK VEHICLE::.</title>

<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../scripts/getModule.js"></script>
<script type="text/javascript" src="../scripts/misc.js"></script>
<script type="text/javascript" src="../scripts/getModal.js"></script>  

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>

<script type="text/javascript">
  
  var driverArray = [];
  var driverNumberArray = [];
  var busdbids = [];

  <?php
  foreach($busArray as $key => $val)
  {
    ?>
    driverArray['<?php echo $val;?>'] = '<?php echo $driverArray[$key];?>';
    driverNumberArray['<?php echo $val;?>'] = '<?php echo $driverMobileArray[$key];?>';
    busdbids['<?php echo $val;?>'] = '<?php echo $idArray[$key];?>';
    <?php

  }
  ?>


</script>
</head>
<body>

<div style="position:fixed;left:-500px;top:20px;height:500px;z-index:2000;background:#fff;width:300px;" class="shadow" id="routeList">

<div style="position:absolute;top:40px;left:30px;width:1px;height:500px;background:#ddd;display:none" id="routeLine"></div>

  <div style="height:40px;width:100%;padding:10px;" class="bg-warning">
  <div style="float:right;padding:1px;cursor:pointer" onclick="$('#routeList').animate({left:'-300px'})">
    <i class="fa fa-remove"></i>
  </div>
    Route Information
  </div>
     <div id="directions-panel" style="width:100%;height:580px;overflow-y:auto;padding:10px;overflow-y:auto;overflow-x:hidden;">


      Routes according to selected stops will appear on Map and the list will appear here.
 


  </div>
</div>
<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText">
    <i class="fa fa-map-marker"></i>&nbsp;&nbsp;Initializing map, please wait..

  </span>
</div>



<div style="position:fixed;top:10px;right:10px;z-index:2000;width:300px;background:#fff;padding:10px;color:#999;font-weight:lighter" class="shadow">
<div onclick="$('#busSelector').fadeIn();" style="cursor:pointer">
<div style="float:right;margin-top:4px;color:#999">
<span style="font-size:12px;" class="label label-info">
<span id="buscount">0</span>/<?php echo count($busArray);?>
</span>&nbsp;&nbsp;
  
</div>
<div id="" style="margin-top:5px;float:left">
<i class="fa fa-bus"></i>&nbsp;&nbsp;
<span id="selectedBus">
Track Bus</span></div>
</div>
<div style="position:absolute;top:40px;left:0px;width:300px;background:#fff;display:none;overflow-x:hidden;overflow-y:auto;max-height:500px;height:auto" id="busSelector">
  <table class="table table-striped table-hovered" style="cursor:pointer">
  <?php
  foreach($busArray as $val)
  {
    ?>
    <tr onclick="firstCall='0';liveBusData('<?php echo $val;?>');$('#busSelector').fadeOut();" class="notlive" id="bus-<?php echo $val;?>">
      <td>
<?php echo $val;?>        
      </td>
    </tr>


    <?php
  }
  ?>
  </table>

</div>
</div>


<div id="map" style="width:100%;height:500px;"></div>

<div style="position:fixed;bottom:-1000px;left:0px;background:#fff;height:200px;width:100%;z-index:2000;padding:20px;border:1px #eee solid;" class="shadow" id='analyser'>
<div style="position:absolute;top:10px;right:10px;cursor:pointer;height:30px;width:30px;text-align:right" onclick="$('#analyser').animate({bottom:'-200px'})">
  <i class="fa fa-remove"></i>
</div>
<table style="width:100%;">
  <tr>
    <td valign="top" style="text-align:left;border-right:1px #eee solid;">
    <div style="float:right;padding-right:20pxcursor:pointer;" onclick="generateRoute();">
      <img src="../images/route.png" alt=""/>
    </div>
    <strong style="font-size:20px;">
  <i class="fa fa-bus"></i>&nbsp;&nbsp;<span id="livename">MP09CM7163</span>
</strong>
<br/>


  
      Driver Details
<hr style="margin:10px !important">
<div class="text-primary" style="font-size:12px;">

<i class="fa fa-user"></i>&nbsp;&nbsp;<span id="livedirver"></span>

<br/>
<i class="fa fa-phone"></i>&nbsp;&nbsp;<span id="livenumber"></span>
<br/>
<br/>
</div>
<button class="btn btn-primary" id="livespeed">13 Km/h</button>

    </td>
    <td colspan="2" style="border-left:1px #eee solid;width:85%;" valign="top">
    <div id="speedchart" style="">
<div class="ct-chart ct-perfect-fourth" style="height:150px;width:100%;"></div> 
<center>
<div style="float:right" onclick="resetChart()">
  <button class="btn btn-sm btn-info"><i class="fa fa-reload"></i>&nbsp;RESERT CHART</button>
</div>
  SPEED CHART
</center>
</div>
    </td>
  </tr>
</table>


</div>

<div id="unableset" style="position:fixed;top:-100px;left:0px;width:100%;background:#fff;">
  <div style="padding:20px;">
  <span class="text-danger">
  
  <i class="fa fa-warning"></i>
    Unable to locate bus at the moment.
  </span>
    <br/>
<span style="font-size:12px;">
    Please wait a few moments. Check other buses in the mean time. <br/>
    </span>

  </div>
</div>
</body>
<script type="text/javascript">
var busHalt = [];
var inHeight = window.innerHeight;
$('#map').animate({height:inHeight},50);
var latArray = [];
var longArray = [];
var busListReq = '<?php echo $busString;?>';
var busArray = busListReq.split(",");

var x = 0;

var firstPan = '0';
var speedArray = [];
var speedLog = [];
var timeLog = [];
var timeArray = [];


var liveShowBus = '0';
var runCount=0;

  function getData()
  {

    
var url = 'http://locate.trackinggenie.com/trackingapi/Get_vehicle_status.php?token=VEdfMTI1OTM=&key=<?php echo $keyString;?>';     
     //var url = 'http://locate.trackinggenie.com/trackingapi/Get_vehicle_status.php?token=VEdfMTI1OTM=&key=<?php echo $keyString;?>&fromtime=2016-11-04%2008:00&totime=2016-11-04%2008:05';   
    
 
    genAjax(url,'',function(response)
    {

      hideToast();
      document.getElementById('buscount').innerHTML = '0';

     try
      {
      response = JSON.parse(response);
      
      for(j=0;j<busArray.length;j++)
      {
        var thisBus = busArray[j];
        document.getElementById('bus-'+busArray[j]).className = 'notlive';

        var thisPointer = response[thisBus];

          if(thisPointer[x].NLangitude != "0.00")
          {

            latArray[busArray[j]] = thisPointer[x].NLangitude;
            longArray[busArray[j]]  = thisPointer[x].ELangitude;  
            speedArray[busArray[j]] = thisPointer[x].Speed; 
            var dtime =    thisPointer[x].Devicetime.split(" ");
            timeArray[busArray[j]] = dtime[1]; 
           // timeArray[busArray[j]] = thisPointer[x].Devicetime; 
            
            if(speedLog[busArray[j]])
            {
              var newVal = speedLog[busArray[j]].length;
              speedLog[busArray[j]][newVal] = speedArray[busArray[j]];
              timeLog[busArray[j]][newVal] = timeArray[busArray[j]];
            }               
            else
            {
              timeLog[busArray[j]] = [];
              speedLog[busArray[j]] = [];
              timeLog[busArray[j]][0] = timeArray[busArray[j]];
              speedLog[busArray[j]][0] = speedArray[busArray[j]];
            }
              if(speedArray[busArray[j]] == 0 )
              {
                busHalt[busArray[j]] = '1';
                document.getElementById('bus-'+busArray[j]).className = 'halt';
              }  
              else
              {
                runCount++;
                busHalt[busArray[j]] = '0';
                document.getElementById('bus-'+busArray[j]).className = 'running';
              }

          }

          

        
      }
        bc = parseInt(document.getElementById('buscount').innerHTML);
                bc = bc+runCount;
                document.getElementById('buscount').innerHTML = bc;

      placeMarker();

     
      
       }
      catch(e)
      {
        console.log(response);
toast("Unable to fetch location of all selected buses at the moment. Either the devices are down or server is experiencing some issues. System will keep retrying, you can still track loaded buses.","toast bg-danger","0");
      }
      
      setTimeout(function(){
       getData();
      },30000);
      
    });
  }


var myLat = localStorage.getItem("myLat");
myLat = parseFloat(myLat);

var myLang = localStorage.getItem("myLang");
myLang = parseFloat(myLang);
     


var infoId;
 
 var marker = [];
 var map;
 var myCity = [];
 var markerBounds;
function myMap() {
         directionsService = new google.maps.DirectionsService;
         //directionsDisplay = new google.maps.DirectionsRenderer;
directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    });

  var mapCanvas = document.getElementById("map");
  var myCenter=new google.maps.LatLng(myLat,myLang);
  var mapOptions = {center: myCenter, zoom: 13,disableDefaultUI:true};
  map = new google.maps.Map(mapCanvas, mapOptions);
  directionsDisplay.setMap(map);
markerBounds = new google.maps.LatLngBounds();
hideToast();
getData();
}


  var direction = 1;
    var rMin = 50, rMax = 200;
    var intVal = 30;

var animCity;
var infowindow = [];

var oldPosLat = [];
var oldPosLang = [];
var travelCords;
var travelPath;
var image;
var imageLive;
function placeMarker() {

    imageLive = {
  url: '../pin-live.svg',
  size: new google.maps.Size(100, 100 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(40,40),
  scaledSize: new google.maps.Size(100, 100)
};


image = {
  url: '../pin.svg',
  size: new google.maps.Size(12, 12 ),
  origin: new google.maps.Point(0, 0),
  anchor: new google.maps.Point(10,10),
  scaledSize: new google.maps.Size(12, 12)
};

for(k=0;k<busArray.length;k++)
{
if(latArray[busArray[k]] && latArray[busArray[k]] != 'undefined')
{
latArray[busArray[k]] = parseFloat(latArray[busArray[k]]);
longArray[busArray[k]] = parseFloat(longArray[busArray[k]]);
thisPoint = new google.maps.LatLng(latArray[busArray[k]],longArray[busArray[k]]);
  if(marker[busArray[k]])
  {


if(speedArray[busArray[k]] == 0)
{

  marker[busArray[k]].setIcon(image);
   infowindow[busArray[k]].setOptions({pixelOffset: new google.maps.Size(0,0)});

}
else
{
  marker[busArray[k]].setIcon(imageLive);
   infowindow[busArray[k]].setOptions({pixelOffset: new google.maps.Size(-10,30)});
}


marker[busArray[k]].setPosition(thisPoint);
driveCircleLine(k);


  }
  else
  {
    oldPosLat[busArray[k]] = parseFloat(latArray[busArray[k]]);
    oldPosLang[busArray[k]] = parseFloat(longArray[busArray[k]]);
        thisLoc = thisPoint;


/*
myCity[busArray[k]] = new google.maps.Circle({
  strokeWeight: 0,
            fillColor: '#fece33',
            fillOpacity: 0.3,
            map: map,
            center: thisLoc,
            radius: 20
          });

*/


if(speedArray[busArray[k]] == 0)
{
toUse =image;
}
else
{
  toUse = imageLive;
}



marker[busArray[k]] = new google.maps.Marker({
    position: thisLoc,
    map: map,
    optimized: false,
    icon:toUse,
    id:busArray[k],
    easing: "easeOutExpo",
    duration:300
  });

//animateRadius(busArray[k]);





google.maps.event.addListener(marker[busArray[k]], 'click', function() {
  infowindow[this.id].open(map,marker[this.id]);
  });
 infowindow[busArray[k]] = new google.maps.InfoWindow({
    content: '<span onclick="firstCall=\'0\';liveBusData(\''+busArray[k]+'\');" style="color:#333;font-size:16px;"><i class="fa fa-bus"></i>&nbsp;&nbsp;'+busArray[k]+'</span>',
    pixelOffset: new google.maps.Size(-10,30),
    id:k
  });

if(speedArray[busArray[k]] == 0)
{
 infowindow[busArray[k]].setOptions({pixelOffset: new google.maps.Size(0,0)});
}




// infowindow[busArray[k]].open(map,marker[busArray[k]]);

  }

//  markerBounds.extend(thisPoint);
}

}

if(firstPan == '0')
{
  firstPan = '1';
  map.panTo(thisLoc);


}

if(liveShowBus != '0')
{
  map.panTo(marker[liveShowBus].getPosition());
}




// map.fitBounds(markerBounds);
}


var firstCall = '0';

function driveCircleLine(k)
{
//myCity[busArray[k]].setCenter(thisPoint);

if(speedArray[busArray[k]] >= 50)
{
  strokeColor = '#ff3333';
}
else
{
  strokeColor = '#076bff';
}
travelCords = [
          {lat: oldPosLat[busArray[k]], lng: oldPosLang[busArray[k]]},
          {lat: latArray[busArray[k]], lng: longArray[busArray[k]]}
        ];
        travelPath = new google.maps.Polyline({
          path: travelCords,
          geodesic: true,
          strokeColor: strokeColor,
          strokeOpacity: 1.0,
          strokeWeight: 3
        });

        travelPath.setMap(map);

oldPosLat[busArray[k]] = latArray[busArray[k]];
oldPosLang[busArray[k]] = longArray[busArray[k]];

if(liveShowBus == busArray[k])
{
liveBusData(liveShowBus);  
}


}


function liveBusData(bus)
{

  if(bus != '0')
  {
 
 if(latArray[bus])
 {
  liveShowBus = bus;
  
  document.getElementById('selectedBus').innerHTML = bus;
    if(speedArray[bus] != 0)
    {
    document.getElementById('livespeed').innerHTML = "SPEED: "+speedArray[bus] + "Km/H";        
    }
    else
    {
          document.getElementById('livespeed').innerHTML = "BUS HALT: "+speedArray[bus] + "Km/H";  
    }

    
  $('#unableset').animate({top:'-100px'});
  
    currentSeries.push(parseInt(speedArray[bus]));
    updateChart(liveShowBus);
    if(speedArray[bus] >= 50 || speedArray[bus] == 0)
    {
      document.getElementById('livespeed').className = "btn btn-danger";
    }
    else
    {
      document.getElementById('livespeed').className = "btn btn-primary";
    }

    document.getElementById('livename').innerHTML = bus;   
    document.getElementById('livedirver').innerHTML = driverArray[bus];   
    document.getElementById('livenumber').innerHTML = driverNumberArray[bus];   
    map.panTo(marker[bus].getPosition());
    if(firstCall == '0')
    {
      firstCall = '1';
      setTimeout(function(){
    map.setZoom(20);
      },500);

    $('#analyser').animate({bottom:'0px'},300);
      infowindow[bus].open(map,marker[bus]);
    }
 
    }  
  else
  {
  $('#unableset').animate({top:'0px'});
  }
}

  } 

function animateRadius(circleid)
{
  

  
       animCity = setInterval(function() {
            var radius = myCity[circleid].getRadius();
            var zoom = map.getZoom();

if(busHalt[circleid]  == '1')
{
  myCity[circleid].setRadius(0);
}
else
{
            if(zoom <= 8)
            {
              rMax = 7200;
              rMin = 5100;
              intVal = 35;
            } 
             if(zoom == 9)
            {
              rMax = 6500;
              rMin = 4500;
              intVal = 30;
            }
            if(zoom == 10)
            {
              rMax = 5000;
              rMin = 3500;
              intVal = 25;
            }
            if(zoom == 11)
            {
              rMax = 2100;
              rMin = 900;
              intVal = 20;
            }

            if(zoom == 12)
            {
              rMax = 1400;
              rMin = 500;
              intVal = 15;
            }                      
            if(zoom == 13)
            {
              rMax = 900;
              rMin = 300;
              intVal = 10;
            }
            else if(zoom == 14)
            {
              rMax = 480;
              rMin = 120;
              intVal = 6;
            }
            else if(zoom == 15)
            {
              rMax = 240;
              rMin = 60;
              intVal = 3;
            }
            else if(zoom == 16)
            {
              rMax = 120;
              rMin = 0;
              intVal = 2;
            }
            else if(zoom == 17)
            {
              rMax = 60;
              rMin = 0;
              intVal = 1;
            }
            else if(zoom == 18)
            {
              rMax = 30;
              rMin = 0;
              intVal = 0.5;
            }
            else if(zoom == 19)
            {
              rMax = 15;
              rMin = 0;
              intVal = 0.25;
            }

            else if(zoom == 20)
            {
              rMax = 7.5;
              rMin = 0;
              intVal = 0.125;
            }


            else if(zoom == 21)
            {
              rMax = 3.75;
              rMin = 0;
              intVal = 0.0625;
            }

            
            if ((radius <= rMax)) {
                myCity[circleid].setRadius(radius + intVal);
                var fillOpacity = 1 - (radius/rMax);
                if(fillOpacity > 0.5)
                {
                  fillOpacity = 0.5;
                }
                myCity[circleid].setOptions({fillOpacity:fillOpacity});
            }
            else 
            {
              myCity[circleid].setRadius(rMin);
            }
          }

        }, 30);

}


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&callback=myMap"></script>

<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/vendor/jquery.easing.1.3.js"></script>



<!-- we use markerAnimate to actually animate marker -->
<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/vendor/markerAnimate.js"></script>

<!-- SlidingMarker hides details from you - your markers are just animated automagically -->
<script src="http://terikon.github.io/marker-animate-unobtrusive/demo/SlidingMarker.js"></script>
<script>
  SlidingMarker.initializeGlobally();
</script>
 <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script src="../scripts/chartist-plugin-tooltip.js"></script>
    <style type="text/css">
      
      .ct-series-a .ct-line {
  /* Set the colour of this series line */
  stroke: #076bff;
  /* Control the thikness of your lines */
  stroke-width: 1px;
  /* Create a dashed line with a pattern */
  
}

.ct-series-a .ct-point {
  /* Colour of your points */
  stroke: #0d59c4;
  /* Size of your points */
  stroke-width: 5px;
  /* Make your points appear as squares */
  stroke-linecap: circle;
}

.ct-label
{
  font-size:10px !important;
}
    </style>

<script type="text/javascript">

var currentSeries = [];
      var data = {
  labels: ['a','b'],
  series: [
    [0,3]
  ]
};
var curChat = new Chartist.Line('.ct-chart', data,{
  lineSmooth: Chartist.Interpolation.simple({
    divisor: 2
  }),
  fullWidth: true,
  plugins: [
    Chartist.plugins.tooltip()
  ]
});




function updateChart(bus)
{
var upArray = [];
var temp = speedLog[bus];
  for(j=0;j<temp.length;j++)
  {
    upArray[j] = [];
upArray[j] = {
  meta:timeLog[bus][j],
  value:speedLog[bus][j]
}
  }

var newData = {
  labels: timeLog[bus],
  series: [
    upArray
  ]
};

curChat.update(newData)

}

var directionsService;
var directionsDisplay;

var generatedRoute = [];
var routedata = [];

function generateRoute()
{
  if(generatedRoute[liveShowBus])
  {
     calculateAndDisplayRoute(directionsService, directionsDisplay);
  }
  else
  {
    url = 'getrouteinfo.php?bus='+busdbids[liveShowBus];
    genAjax(url,'',function(response)
   {
    if(response.indexOf('NOTHING FOUND') != -1)
    {
      toast("Route not available in database. Please check you have alloted a route to this BUS #"+liveShowBus+" in database",'toast bg-danger','10000');
    }
    else
    {
    generatedRoute[liveShowBus] = response;
     calculateAndDisplayRoute(directionsService, directionsDisplay);
    }
    });
  }
 
}




var routeMarkers = [];

   function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
    
for(j=0;j<routeMarkers.length;j++)
{
  routeMarkers[j].setMap(null);
}


    dataSet = JSON.parse(generatedRoute[liveShowBus]);
    for(j=0;j<dataSet.length;j++)
    {
      thisResp = dataSet[j];
      if(thisResp.type == 'start')
      {
        startPoint = thisResp.lat+","+thisResp.lng;
        tt = startPoint.split(",");
      }
      else if(thisResp.type == 'end')
      {
        endPoint = thisResp.lat+","+thisResp.lng;
        tt = endPoint.split(",");
      }
      else
      {
        wayPoint = thisResp.lat+","+thisResp.lng;
        tt = wayPoint.split(",");
          waypts.push({
              location: thisResp.lat+","+thisResp.lng,
              stopover: true
            });
      }
thisLoc = new google.maps.LatLng(parseFloat(tt[0]),parseFloat(tt[1]));

      routeMarkers[j] = new google.maps.Marker({
    position: thisLoc,
    map: map,
    optimized: false,
    icon:'../images/pin-dir.png',
    easing: "easeOutExpo",
    duration:300
  });


    }



        directionsService.route({
          origin: startPoint,
          destination: endPoint,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            map.setCenter(new google.maps.LatLng(parseFloat(tt[0]),parseFloat(tt[1])));
            setTimeout(function(){
            map.setZoom(14);              
          },300);




            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            html = '';
            totalDistance = 0;
            for (var i = 0; i < route.legs.length; i++) {
              var thisLab = i+1;
             var routeSegment = i + 1;

       html += '<div style="position:relative;width:100%;">';
    if(i == 0)
      {
      html += '<img src="../images/pin-dir.png" style="position:absolute;top:0px;left:10px;width:20px;"alt=""/><div style="padding-left: 40px;min-height:40px">';
     html += route.legs[i].start_address; 
      html += '</div>';
      }

      html += '<div style="position:relative;width:100%;"><span class="label label-success" style="position:absolute;top:10px;left:0px">';
      html += route.legs[i].distance.text;
      html += '</span></div><br/><br/><div style="position:relative;width:100%;padding-left:40px"><img src="../images/pin-dir.png" style="position:absolute;top:0px;left:10px;width:20px;"alt=""/></div><div style="padding-left: 40px;;min-height:40px">';
       html += route.legs[i].end_address ;   
      html += '</div></div>';
      tdest = parseFloat(route.legs[i].distance.text);
      tdest = Math.round( tdest * 10 ) / 10;
totalDistance = tdest+totalDistance;




            }


                 summaryPanel.innerHTML = html;
              document.getElementById('routeLine').style.display= 'block';
              $('#routeList').animate({left:'20px'});
              totalDistance = Math.round( totalDistance * 10 ) / 10;


             
          } else {
            console.log("Not Working");
          }
        });
      }

var toPutheight = window.innerHeight - 220;
var toPutheightLower = window.innerHeight - 260;
$('#routeList').animate({height:toPutheight});
$('#directions-panel').animate({height:toPutheightLower});
$('#routeLine').animate({height:toPutheightLower});


function resetChart()
{
  var tempSpeedlog = [];
  tempSpeedlog[liveShowBus] = [];
  var temptimelog = [];
  temptimelog[liveShowBus] = [];
  var k=0;
  console.log(speedLog[liveShowBus]);
  for(j=0;j<speedLog[liveShowBus].length;j++)
  {


    
    if(j >= (speedLog[liveShowBus].length - 5))
    {
      tempSpeedlog[liveShowBus][k] = speedLog[liveShowBus][j];
      temptimelog[liveShowBus][k] = timeLog[liveShowBus][j];
      k++;
    }
    
  }
  if(k != 0)
  {
speedLog[liveShowBus] = tempSpeedlog[liveShowBus];
timeLog[liveShowBus] = temptimelog[liveShowBus];
updateChart(liveShowBus);    
  }

}

    </script>



</html>
