<?php
include("../include/connection.php");
$busid = $_REQUEST['bus'];
$fordate = $_REQUEST['fordate'];
$fromdate = $fordate." 00:00:00";
$todate = $fordate." 23:59:59";


$getData  = mysqli_query($con,"SELECT * FROM `buses` WHERE `id` = '$busid'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$busName = $row['bus'];
$getList = mysqli_query($con,"SELECT * FROM `busgps` WHERE `busid` = '$busid' AND `devicetime` BETWEEN '$fromdate' AND '$todate'") or die(mysqli_error($con));

$data = Array();
$thisData = Array();
$k=0;

?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
<link rel="stylesheet" type="text/css" href="/css/result-light.css">
<style type="text/css"></style>
<title></title>

<script type="text/javascript">
var xAxisArray = [];
var seriesArray = [];
    <?php
    $j=0;
while($row = mysqli_fetch_array($getList))
{
    $temp = $row['devicetime'];
    $temp = explode(" ",$temp);
    $temp1 = explode(":",$temp[1]);
    $temp2 = $temp1[0].":".$temp1[1];
?>
xAxisArray[<?php echo $j;?>] = '<?php echo $temp2;?>'; 
seriesArray[<?php echo $j;?>] = <?php echo $row['speed'];?>;
<?php
$j++;
}

  
    ?>

</script>






<script type='text/javascript'>//<![CDATA[

// Data retrieved from http://vikjavev.no/ver/index.php?spenn=2d&sluttid=16.06.2015.
$(function () {
    Highcharts.chart('container', {
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Speed Chart For <?php echo $busName;?>'
        },
        subtitle: {
            text: 'Data for <?php echo date("d, M y",strtotime($fordate));?>'
        },
        xAxis: {
            categories: xAxisArray,
            labels:{
                enabled:false
            }
        },
        yAxis: {
            title: {
                text: 'Speed Kmph'
            },
            minorGridLineWidth: 0,
            gridLineWidth: 0,
            alternateGridColor: null,
            plotBands: [{ 
                from: 45,
                to: 500,
                color: 'rgba(231, 76, 60, 0.4)',
                label: {
                    text: 'SPEED ALERT',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        tooltip: {
            valueSuffix: ' Kmph',
            enabled: true,
    formatter: function() {
                return '<b>'+ this.y +' Kmph</b><br/>'+this.x;

                  }
        },
        plotOptions: {
            spline: {
                lineWidth: 4,
                states: {
                    hover: {
                        lineWidth: 5
                    }
                },
                marker: {
                    enabled: true
                },
            }
        },
        series: [{
            name: '<?php echo $busName;?>',
            data: seriesArray

        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }
        }
    });
});
//]]> 

</script>

  
</head>

<body>
  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div style="float:right">
    <button class="btn btn-sm btn-primary">
                <i class="fa fa-line-chart"></i>&nbsp;CHART
    </button>
    &nbsp;&nbsp;
    <button class="btn btn-sm btn-primary">
                <i class="fa fa-th-large"></i>&nbsp;DATA TABLE
    </button>

</div>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

  
</body>

</html>



