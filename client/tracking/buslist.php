<?php
include("../include/config.php");
?>
<div class="moduleHead">
<div class="moduleHeading">
Reporting & Analysis
</div>
</div>
<br/>

<div class="row">

<div class="col-sm-3">


<div class="tabelContainer" style="height:auto;">
<table class="table table-striped table-hover operatelist shadow" style="border:1px #eee solid;" cellpadding="0" cellspacing="0">
<tr id="opSel0" class="selectedred"><td onclick="selectOperation(0);"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;&nbsp;&nbsp;Live Track</td></tr>
<tr id="opSel1"><td onclick="selectOperation(1);"><i class="fa fa-play"></i>&nbsp;&nbsp;&nbsp;&nbsp;Replay Tracking</td></tr>
<tr id="opSel2"><td onclick="selectOperation(2);"><i class="fa fa-area-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;Speed Chart</td></tr>
<tr id="opSel3"><td onclick="selectOperation(3);"><i class="fa fa-dot-circle-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;Halt Summary</td></tr>
<tr id="opSel4"><td onclick="selectOperation(4);"><i class="fa fa-bar-chart"></i>&nbsp;&nbsp;&nbsp;&nbsp;Activity Reports</td></tr>

</table>
</div>
	</div>



	<div class="col-sm-9">
	

<div style="border:1px #eee solid;">


<div id="rep0">
	<?php include("livetrack-filter.php"); ?>
</div>


<div id="rep1" style="display:none">
	<?php include("replay-filter.php"); ?>
</div>



<div id="rep2" style="display:none">
	<?php include("speedChart.php"); ?>
</div>


<div id="rep3" style="display:none">
	<?php include("haltFilter.php"); ?>
</div>




<div id="rep4" style="display:none">
	<?php include("activity-filter.php"); ?>
</div>

</div>





</div>


<br/><br/><br/><br/><br/><br/><br/><br/><br/>