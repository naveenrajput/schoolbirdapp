<div class="row">
	<div class="col-sm-3" style="padding:0px ;">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0" style="margin-bottom:0px !important">
<tr>
<th>

<div id="selectHolder">
Select Buses

<div style="float:right;color:#aaa;">

<div style="display:inline-block;text-align:right;" onclick="$('#searchHolder').show();">
	<i class="fa fa-search" data-toggle="tooltip" title="Search Bus"></i></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<div style="display:inline-block;text-align:right"  onclick="selectAll(this);" lang="0">
	<i class="fa fa-check-square-o" data-toggle="tooltip" title="Select All" ></i></div>
</div>
</div>

<div id="searchHolder" style="position:relative;display:none">

<hr>
<div style="position:absolute;top:30px;right:10px;z-index:200" onclick="document.getElementById('searchBusInput').value = '';searchBusTable('')">
	<i class="fa fa-remove" style="color:#ccc"></i>
</div>
		<div class="w3-group margin10" style="width:100%;margin:25px 0px 5px 0px !important">      
		      <input class="w3-input input1bdark" type="text" style="width:100%" id="searchBusInput" name="date" required="" onkeyup="searchBusTable(this.value)">
		      <label class="w3-label w3-label-custom" style="font-size:11px !important;">
		     Enter Search Term</label>
		</div>

</div>
</th>
</tr>
</table>
<div class="tabelContainer shadow" style="height:auto;overflow-y:scroll;overflow-x:hidden;maring-top:0px !important;" id="busLister">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<?php
$getData = mysqli_query($con,"SELECT * FROM `buses` ORDER BY `bus` ASC") or die(mysqli_error($con));
$j=0;
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="buslistRow<?php echo $j;  ?>" onclick="toogleSelect(this)" lang="0" data-value="<?php echo $row['id'];?>">
<td class="text-primary"  id="buslistTd<?php echo $j;  ?>" ><?php echo $row['bus'];?></td>
</tr>
<?php
$j++;
}
?>
</table>
<input type="" id="totalBusCount" value="<?php echo $j;?>" name="" style="display:none">
</div>
	</div>
	<div class="col-sm-9">
<div style="float:right;padding:20px;text-align:right" onclick="liveTrack()">
							<button class='btn btn-primary'>TRACK BUSES <i class="fa fa-arrow-right"></i></button>
			</div>
		<div style="padding-top:100px;">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp; Select buses from the list.
			<br/>
			<br/>
			


		</div>
	</div>
</div>
