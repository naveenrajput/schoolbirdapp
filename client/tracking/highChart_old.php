<?php
include("../include/connection.php");
$busid = $_REQUEST['bus'];
$fordate = $_REQUEST['fordate'];
$fromdate = $fordate." 00:00:00";
$todate = $fordate." 23:59:59";
$getList = mysqli_query($con,"SELECT * FROM `busgps` WHERE `busid` = '$busid' AND `devicetime` BETWEEN '$fromdate' AND '$todate'") or die(mysqli_error($con));

$data = Array();
$thisData = Array();
$k=0;

?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
<link rel="stylesheet" type="text/css" href="/css/result-light.css">
<style type="text/css"></style>
<title></title>

<script type="text/javascript">
var xAxisArray = [];
var seriesArray = [];
    <?php
    $j=0;
while($row = mysqli_fetch_array($getList))
{
    $temp = $row['devicetime'];
    $temp = explode(" ",$temp);
    $temp1 = explode(":",$temp[1]);
    $temp2 = $temp1[0].":".$temp1[1];
?>
xAxisArray[<?php echo $j;?>] = '<?php echo $temp2;?>'; 
seriesArray[<?php echo $j;?>] = <?php echo $row['speed'];?>;
<?php
$j++;
}
  
    ?>

</script>
<script type='text/javascript'>//<![CDATA[

$(function () {
    Highcharts.chart('container', {
        title: {
            text: 'Monthly Average Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: xAxisArray
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'MP09CM7163',
            data: seriesArray
        }]
    });
});
//]]> 

</script>

  
</head>

<body>
  <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

  
</body>

</html>

