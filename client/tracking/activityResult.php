<?php
include("../include/connection.php");
$busid = $_REQUEST['bus'];
$fordate = $_REQUEST['fordate'];
$fromdate = $fordate." ".$_GET['fromtime'];
$todate = $fordate." ".$_GET['totime'];


$getData  = mysqli_query($con,"SELECT * FROM `buses` WHERE `id` = '$busid'") or die(mysqli_error($con));
$row = mysqli_fetch_array($getData);
$busName = $row['bus'];
$getList = mysqli_query($con,"SELECT * FROM `busgps` WHERE `busid` = '$busid' AND `devicetime` BETWEEN '$fromdate' AND '$todate'") or die(mysqli_error($con));

$data = Array();
$thisData = Array();
$k=0;

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../scripts/misc.js"></script>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" media="screen" />
<script src="https://use.fontawesome.com/dc4f36d12e.js"></script>

<style type="text/css"></style>
<title></title>

<script type="text/javascript">
var xAxisArray = [];
var seriesArray = [];
    <?php
    $j=0;
    $deviceArray = Array();
    $speedArray = Array();
    $ltlng = Array();
    $alert = Array();
    $halt = Array();
while($row = mysqli_fetch_array($getList))
{

    $temp = $row['devicetime'];
    $temp = explode(" ",$temp);
    $temp1 = explode(":",$temp[1]);
    $temp2 = $temp1[0].":".$temp1[1];
    $deviceArray[] .= $row['devicetime'];
    $alert[] .= $row['statusreason'];
    $speedArray[] .= $row['speed']; 
    $halt[] .=$row['halt'];
        $ltlng[] .= $row['lat'].",".$row['lng'];
$j++;
}

  
    ?>

</script>





  
</head>

<body>




<div id="containerTab" style="width: 100%; height: 400px; margin: 0 auto;overflow-y:auto;">
    <table class="table table-striped table-hovered fetch">
        <tr>
            <th>#</th><th>Speed</th><th>Time</th><th>Halt</th><td>Alert</td><td>Address</td>
        </tr>
        <?php
        foreach($speedArray as $key => $val)
        {
            ?>
            <tr class="<?php if($val >= 50) echo "bg-danger" ;?>">
                <td><?php echo $key+1;?></td>
                <td><?php echo $val;?></td>
                <td><?php echo $deviceArray[$key];?></td>
                <td><?php echo $alert[$key];?></td>
                <td><?php if($halt[$key] == '1')
                {
                    ?>
                    <button class="btn btn-sm btn-primary">
                    <i class="fa fa-alert"></i>&nbsp;&nbsp;BUS HALT
                    </button>
                    <?php
                }
?>
                </td>
                <td class="text-default"  lang="<?php echo $ltlng[$key];?>" onclick="getAddress(this)">View Address</td>

            </tr>
            <?php
        }
        ?>
    </table>
</div>
  
<script type="text/javascript">
    window.top.window.hideProcessing(); 
</script>  
</body>

</html>



