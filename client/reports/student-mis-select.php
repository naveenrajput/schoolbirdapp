<?php
include("../include/config.php");
?>
<div style="padding:20px;">

<form action="reports/student-mis.php" target="_blank" method="post">
<div class="row">

<div class="col-sm-12" style="padding:0px ;">

<div class="w3-group margin10" style="width:100%">  

    <select class="w3-input input1bdark" style="width:100%;height:47px" id="actBus" name="class" required=""  name="req">
    <option value="ALL">ALL CLASSES</option>
    <?php
    $getData = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name` ASC") or die(mysqli_error($con));
    while($row = mysqli_fetch_array($getData))
    {
      ?>
      <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
      <?php     
    }
    ?>
    </select>  
          <label class="w3-label w3-label-custom" style="font-size:11px !important;">
         Select Class</label>
    </div>
</div>



<div class="col-sm-12" style="padding:0px ;">

 Select Export Fields
 <br/>
 <br/>

    <select class="input" style="width:100%;" id="actBus" multiple="multiple" size="10" name="fields[]" required=""  name="req">
    <option value="stid">Student Id</option>
    <option value="name">Name</option>
    <option value="class">Class & Section</option>
    <option value="address">Address</option>
    <option value="emergency">Emergency Number</option>
    <option value="father">Father Name</option>
    <option value="mother">Mother Name</option>
    <option value="sms">SMS Activated</option>
    <option value="stop">Stop Name</option>
    <option value="route">Weekday Route</option>
    <option value="routereverse">Weekday Route Afternoon</option>
    <option value="routesat">Saturday Morning</option>
    <option value="routesatrev">Saturday Afternoon</option>
    </select>  
    
</div>
<div class="col-sm-12">
  <button class="btn btn-primary" onclick="">EXPORT&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></button>
</div>
  </div>

  <form>


</div>