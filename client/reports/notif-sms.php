<?php
include("../include/config.php");
$getData = mysqli_query($con,"SELECT student.stid,student.name,parents.mobile,parents.father,class.name,student.logged FROM student,class,parents WHERE  student.class = class.id AND student.parentid = parents.id ORDER BY student.name ASC") or die(mysqli_error($con));
$name = "Notification-vs-SMS.xls";
header("Content-Disposition: attachment; filename=\"$name\"");
header("Content-Type: application/vnd.ms-excel");

?>
<table border="1">
<tr>
	<th>Student Id</th>
	<th>Student Name</th>
	<th>Student Mobile</th>
	<th>Father Name</th>
	<th>Class</th>
	<th>Notification Type</th>
	
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
	?>
	<tr>
<td><?php echo $row[0];?></td>
<td><?php echo $row[1];?></td>
<td><?php echo $row[2];?></td>
<td><?php echo $row[3];?></td>
<td><?php echo $row[4];?></td>
<td><?php 
if($row[5] == '1')
{
	echo "Mobile app notification";
}
else
{
	echo "SMS on mobile number";
}
?></td>
</tr>
	<?php
}
?>
</table>