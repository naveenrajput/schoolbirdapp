<?php
include("../../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `class` ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
  <button class="btn btn-sm btn-danger"  onclick="getModule('setup/classes/new.php','formDiv','tableDiv','loading')">+1 ADD NEW CLASS</button>
</div>

<div class="moduleHeading">
Classes
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>Class Teacher</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary" onclick="getModule('setup/classes/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['name'];?></td>
<td><?php echo $row['classteacher'];?></td>

</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>