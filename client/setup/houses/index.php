<?php
include("../../include/config.php");
$getData = mysqli_query($con,"SELECT * FROM `houses` ORDER BY `name` ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
  <button class="btn btn-sm btn-danger"  onclick="getModule('setup/houses/new.php','formDiv','tableDiv','loading')">+1 ADD NEW HOUSE</button>
</div>

<div class="moduleHeading">
Houses
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>Remarks</th>
</tr>
<?php
while($row = mysqli_fetch_array($getData))
{
?>
<tr id="tableRow<?php echo $row['id'];?>">
<td><?php echo $j+1;?></td>
<td class="text-primary" onclick="getModule('setup/houses/edit.php?id=<?php echo $row['id'];?>','formDiv','tableDiv','loading')"><?php echo $row['name'];?></td>
<td>
	<?php echo $row['remark'];?>

</td>
</tr>
<?php
$j++;
}
?>
</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>